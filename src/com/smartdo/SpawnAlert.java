//package com.smartdo;
//
//import android.app.Notification;
//import android.app.NotificationManager;
//import android.app.PendingIntent;
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.os.Bundle;
//import android.util.Log;
//
//public class SpawnAlert extends BroadcastReceiver {
//
//
//
//	@Override
//	public void onReceive(Context context, Intent intent) {
//		Log.i("In", "SPAWNALERT");
//		Intent i = intent;
//		Bundle b = i.getExtras();
//		TodoItem todo = (TodoItem) b.getSerializable("Todo");
//		Log.e("MMMMM",todo.getTitle());
//		NotificationManager nm;
//		nm=(NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
//		Notification n = new Notification(R.drawable.ic_launcher, todo.getTitle(), System.currentTimeMillis());
//		
//		Intent notificationIntent = new Intent(context, AlertAct.class);
//		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//		notificationIntent.putExtra("Todo", todo);
//		Log.i("ENTERING::::", "" +  b.getBoolean("entering"));
//		notificationIntent.putExtra("entering", b.getBoolean("entering"));
//		
//		PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
//		n.setLatestEventInfo(context, todo.getTitle(), todo.getAuthor(), contentIntent);	
//		nm.notify(0,n);				
//	}
//
//
//
//}

package com.smartdo;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

/**
 * Broadcast receiver for detecting a broadcast intended to push a notification to the notification manager / notification bar
 * 
 * @author aref
 *
 */

public class SpawnAlert extends BroadcastReceiver {



	@Override
	public void onReceive(Context context, Intent intent) {
		Log.i("In", "SPAWNALERT");
		Intent i = intent;
		Bundle b = i.getExtras();
		TodoItem todo = (TodoItem) b.getSerializable("Todo");
		
		String key = LocationManager.KEY_PROXIMITY_ENTERING;
        Boolean entering = i.getBooleanExtra(key,  false);
        Log.e("inSPAWNALERT - TITLE:",todo.getTitle());
        Log.e("inSPAWNALERT - TYPE:",""+i.getStringExtra("Type"));
        Log.e("inSPAWNALERT - ENTERING:",""+entering);

         
		NotificationManager nm;
		nm=(NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification n = new Notification(R.drawable.ic_launcher, todo.getTitle(), System.currentTimeMillis());
		n.defaults = Notification.DEFAULT_ALL;

		Intent notificationIntent =intent;
		notificationIntent.setClass(context, AlertAct.class);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
		int rcode=0;

		
		if(intent.getStringExtra("Type").equals("ALARM")){
			rcode=todo.sno1;
			Log.i("SpawnAlert","rcode -- alarm " +rcode + "  "+todo.title);
		}
		else if (intent.getStringExtra("Type").equals("GPS")){
			rcode=todo.sno2;
			Log.i("SpawnAlert","rcode -- gps " + rcode+ "  "+todo.title);
		}
		else{
			Log.i("SpawnAlert","rcode -- else");
		}
		
		//Intent notificationIntent = new Intent(context, AlertAct.class);
		//notificationIntent.putExtra("Todo", todo);
		rcode=rcode*3/2;
		notificationIntent.putExtra("rcode", rcode);
		
		PendingIntent contentIntent = PendingIntent.getActivity(context, rcode, notificationIntent,  0);
		n.setLatestEventInfo(context, todo.getTitle(), todo.getAuthor(), contentIntent);	
		nm.notify(rcode,n);
	}

}

