//package com.smartdo;
//
//import android.app.AlarmManager;
//import android.app.PendingIntent;
//import android.content.Context;
//import android.content.Intent;
//import android.util.Log;
//
//
//public class AlaManager {
//
//	public static void addAlarm(Context c, TodoItem todo){
//		AlarmManager am;
//		am = (AlarmManager) c.getSystemService(Context.ALARM_SERVICE);
//		Intent intent = new Intent("com.spawnalert");
//		intent.putExtra("Todo", todo);
//		intent.putExtra("Type", "ALARM"); 	//0 - GPS trigger
//		PendingIntent proximityIntent = PendingIntent.getBroadcast(c,0, intent, 0);
//		Log.i("YOOO2", ((TodoItem)intent.getSerializableExtra("Todo")).getTitle());
//		
//
//		am.set(AlarmManager.RTC_WAKEUP, todo.getDue_date().getTime(), proximityIntent);
//	}
//	
//	public static void remAlarm(Context c, TodoItem todo){
//		AlarmManager am;
//		am = (AlarmManager) c.getSystemService(Context.ALARM_SERVICE);
//		Intent intent = new Intent("com.spawnalert");
//		intent.putExtra("Todo", todo);
//		intent.putExtra("Type", "ALARM"); 	//0 - GPS trigger
//		PendingIntent proximityIntent = PendingIntent.getBroadcast(c,0, intent, 0);
//		Log.i("DEL", intent.getExtras().getString("Type"));
//
//		am.cancel(proximityIntent);
//	}
//}

package com.smartdo;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

/**
 * This class implements the methods which sets the alarm alerts, removes previously added alerts and also recurrent alarms used for GPS polling.
 * @author Aref
 */

public class AlaManager {

	/**
	 * This method uses a PendingIntent for proximity alerts and sets the alarm
	 * using AlarmManager
	 * @author Aref
	 * @param context
	 * @param todoItem
	 */
	public static void addAlarm(Context context, TodoItem todoItem){
		AlarmManager am;
		am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent("com.spawnalert");
		intent.putExtra("Todo", todoItem);
		//intent.setData(Uri.parse(""+MainActivity.counter2++));
		intent.putExtra("Type", "ALARM"); 	//0 - GPS trigger
		Toast.makeText(context, "1:"+todoItem.getSno1()+"2:"+todoItem.getSno2(), Toast.LENGTH_SHORT).show();

		PendingIntent proximityIntent = PendingIntent.getBroadcast(context,todoItem.getSno1(), intent,0);
		Log.i("YOOO2", ((TodoItem)intent.getSerializableExtra("Todo")).getTitle());
		am.set(AlarmManager.RTC_WAKEUP, todoItem.getDue_date().getTime(), proximityIntent);
	} 
	/**
	 * This method uses a PendingIntent for proximity alerts and sets the alarm
	 * using AlarmManager
	 * @author Aref
	 * @param context
	 * @param automationItem
	 */
	public static void addAlarm(Context context, AutomationItem automationItem){
		AlarmManager am;
		am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);		
		Intent intent = new Intent("com.autoprocess");
		intent.putExtra("AutomationItem", automationItem);
		intent.putExtra("CurrentType", AutomationItem.ALARM_TRIGGER); 	
		Log.e("AlaManager -> addAlarm", "AddAlarmAutoI:"+automationItem.title+"SNO1:"+automationItem.sno1+"SNO2:"+automationItem.sno2);
		PendingIntent proximityIntent = PendingIntent.getBroadcast(context,automationItem.sno1, intent,0); 
		Long time = Long.parseLong(automationItem.triggers.get(AutomationItem.ALARM_TRIGGER));
		am.setRepeating(AlarmManager.RTC_WAKEUP, time, AlarmManager.INTERVAL_DAY,  proximityIntent);
	}
	/**
	 * This method removes the alarm and proximity alert when a task is done or deleted
	 * @author Aref
	 * @param context
	 * @param todoItem
	 */
	public static void remAlarm(Context context, TodoItem todoItem){
		AlarmManager am;
		am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent("com.spawnalert");
		intent.putExtra("Todo", todoItem);
		intent.putExtra("Type", "ALARM"); 	//0 - GPS trigger
		PendingIntent proximityIntent = PendingIntent.getBroadcast(context,todoItem.getSno1(), intent, 0);
		Log.i("DEL", intent.getExtras().getString("Type"));

		am.cancel(proximityIntent);
	}
	/**
	 * This method polls the pending intent every 'Xminutes'
	 * @param context
	 * @param pendingInten
	 * @param Xminutes
	 */
	public static void doEveryXMin(Context context, PendingIntent pendingInten, int Xminutes){
		Log.i("In doEveryXMin", "Will POLL");
		AlarmManager alarm = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		alarm.cancel(pendingInten);
	    alarm.setRepeating(AlarmManager.RTC, System.currentTimeMillis(),Xminutes*60*1000,  pendingInten);
	}
	
}

