//package com.smartdo;
//
//import java.util.ArrayList;
//
//import android.app.Activity;
//import android.content.Context;
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v4.app.NavUtils;
//import android.util.Log;
//import android.view.Menu;
//import android.view.MenuItem;
//import android.view.View;
//import android.widget.ListView;
//import android.widget.TextView;
//
//import com.smartdo.files.TodoFileProcessor;
//
//public class MainActivity extends Activity {
//	public TodoAdapter adapter;
//	ListView todoList;
//	ArrayList<TodoItem> todos;
//	Context context=this;
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_main);
//		TodoFileProcessor.getTodos(this);
//		todos=TodoFileProcessor.todoItems;
//		todoList=(ListView) findViewById(R.id.todoList);
//		adapter=new TodoAdapter(this);
//		todoList.setAdapter(adapter);
//	}
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		switch (item.getItemId()) {
//		case android.R.id.home:
//			// This ID represents the Home or Up button. In the case of this
//			// activity, the Up button is shown. Use NavUtils to allow users
//			// to navigate up one level in the application structure. For
//			// more details, see the Navigation pattern on Android Design:
//			//
//			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
//			//
//			NavUtils.navigateUpFromSameTask(this);
//			return true;
//			
//		case R.id.Add_To_Do_option:
//			viewAddTodo();
//		}
//		return super.onOptionsItemSelected(item);
//	}
//
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.activity_main, menu);
//		return true;
//	}
//	
//	public void todoDetails(View view){
//		Intent intent=new Intent (this, TodoDetails.class);
//		TextView title=(TextView)view;
//		String title1=(String) title.getText();
//		for(int i=0;i<todos.size();i++){
//			if(todos.get(i).getTitle().equals(title1)){
//				intent.putExtra("todo", todos.get(i));
//			}
//		}
//		startActivity(intent);
//	}
//	
//	public void automation_home(View view){
//		try{
//		Intent intent = new Intent(this, Automation_home.class);
//		startActivity(intent);
//		}catch(Exception e){Log.d("error", e.toString());}
//	}
//	
//	public void viewAddTodo(){
//		Intent intent=new Intent(this, AddToDo.class);
//		startActivityForResult(intent, 1001);
//	}
//	 
//	
//	
//	@Override
//	protected void onActivityResult(int requestCode, int resultCode, Intent data){
//		if(resultCode==1002 && requestCode==1001){
//			finish();
//			startActivity(getIntent());
//		}
//	}
//	
//	public void test1(View view){
//		Intent intent=new Intent(this, Test1.class);
//		intent.putExtra("todos", todos);
//		startActivity(intent);
//	}
//	
//	public void showMap(View view){
//		Intent intent=new Intent(this, MapProcessor.class);
//		intent.putExtra("action", "overlays");
//		startActivityForResult(intent, 2001);
//	}
//}

package com.smartdo;

import java.util.ArrayList;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import com.smartdo.files.AutoFileProcessor;
import com.smartdo.files.BookmarkFileProcessor;
import com.smartdo.files.MyNumberFileProcessor;
import com.smartdo.files.TodoFileProcessor;

/**
 * This is main Activity which is started on application launch.
 * It initializes all the important services and files
 * @author amrit, aref, ashish
 */

public class MainActivity extends Activity {

	private GestureDetector gestureDetector;

	
	public static int counter1=0;
	public static int counter2=10;
	
	public TodoAdapter adapter;
	ListView todoList;
	ArrayList<TodoItem> todos;
	Context context=this;
	EditText newTitle;
	String flingdir = "";

	@SuppressWarnings("deprecation")
	@Override
	/**
	 * 
	 * @param savedInstanceState
	 * @return void
	 */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		
		String anim = ""+getIntent().getStringExtra("anim");

		
		if(anim.equals("left")){
			flingdir="left";
			overridePendingTransition(R.anim.rightflingin,R.anim.rightflingout);
		}
		else if (anim.equals("right")){
			flingdir="right";
			overridePendingTransition(R.anim.leftflingin,R.anim.leftflingout);
		}
		else{
			overridePendingTransition(0,0);
		}

		gestureDetector = new GestureDetector(
                new SimpleOnGestureListener(){
               	 private static final int SWIPE_MIN_DISTANCE = 120;
               	    private static final int SWIPE_MAX_OFF_PATH = 200;
               	    private static final int SWIPE_THRESHOLD_VELOCITY = 200;

               	    @Override
               	    public boolean onFling(MotionEvent e1, MotionEvent e2,
               	                         float velocityX, float velocityY) {
               	      try {
               	        float diffAbs = Math.abs(e1.getY() - e2.getY());
               	        float diff = e1.getX() - e2.getX();

               	        if (diffAbs > SWIPE_MAX_OFF_PATH)
               	          return false;
               	        
                	        // Left swipe
                	        if (diff > SWIPE_MIN_DISTANCE
                	        && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                  	           automationHome(null);

                	        // Right swipe
                	        } else if (-diff > SWIPE_MIN_DISTANCE
                	        && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                 	           showMap(null);
                	        }
                	      } catch (Exception e) {
                	        Log.e("MainActivity", "Error on gestures");
                	      }
                	      return false;
                	    }
                	  }
                );
		
		setContentView(R.layout.activity_main);
				
		newTitle=(EditText)findViewById(R.id.addTodoBox);
	
		sendBroadcast(new Intent("smartdo.STARTGPSCHECK")); //mainactivity uses service starter, BookmarksFileProcessor, TodoFileProcessor, AutoFileProcessor
		BookmarkFileProcessor.getBookmarks(this);
		TodoFileProcessor.getTodos(this);
		AutoFileProcessor.getAutos(this);
		todos=TodoFileProcessor.todoItems;
		todoList=(ListView) findViewById(R.id.todoList);
		adapter=new TodoAdapter(this);
		todoList.setAdapter(adapter);
		newTitle.setText("");
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
			
		case R.id.Add_To_Do_option:
			//viewAddTodo();
			
		case R.id.set_number:
			viewAddNumber();
		
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	  public boolean onTouchEvent(MotionEvent event) {
		Log.i("motion", "yup");
	    if (gestureDetector.onTouchEvent(event)) {
	      return true;
	    }
	    return super.onTouchEvent(event);
	  }
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	/**
	 * @author amrit
	 * @param view
	 * @return void
	 * This method starts an Intent for viewing the details of selected Todo.
	 */
	public void todoDetails(View view){
		Intent intent=new Intent (this, TodoDetails.class);
		TextView title=(TextView)view;
		String title1=(String) title.getText();
		TodoItem temp=null;
		for(int i=0; i<TodoFileProcessor.todoItems.size();i++){
			if(TodoFileProcessor.todoItems.get(i).title.equals(title1)){
				temp=TodoFileProcessor.todoItems.get(i);
			}
		}
		intent.putExtra("todo", temp);
		startActivity(intent);
	}
	
	/**
	 * @author ashish
	 * @param view
	 * @return void
	 * This method starts an Intent to go to automation home
	 */
	public void automationHome(View view){
		try{
		Intent intent = new Intent(this, AutomationHome.class);
		intent.putExtra("anim","right");
		startActivity(intent);
		}catch(Exception e){Log.d("error", e.toString());}
	}
	
	/**
	 * @author amrit
	 * @param View
	 * @return void
	 * This method starts an Intent to add new todo
	 */
	public void viewAddTodo(View view){
		Intent intent=new Intent(this, AddToDo.class);
		intent.putExtra("title", newTitle.getText().toString());
		startActivityForResult(intent, 1001);
	}
	@Override
	/**
	 * 
	 * @param requestCode
	 * @param resultCode
	 * @param data
	 */
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		if(resultCode==1002 && requestCode==1001){
			adapter.notifyDataSetChanged();
		}
		newTitle.setText("");
	}
	
	public void test1(View view){
		Intent intent=new Intent(this, Test1.class);
		intent.putExtra("todos", todos);
		startActivity(intent);
	}
	

	
	public void showMap(View view){
		Intent intent=new Intent(this, MapProcessor.class);
		intent.putExtra("action", "overlays");
		intent.putExtra("anim","left");
		intent.putExtra("caller","todo");
		startActivityForResult(intent, 2001);
	}
	
//	@Override
//	public void onBackPressed() {
//		// TODO Auto-generated method stub
//	//	super.onBackPressed();
//		moveTaskToBack(true);
//	
//	}
	
	public void viewAddNumber(){
		Intent intent=new Intent(this, MyNumber.class);
		startActivity(intent);
	}

}

