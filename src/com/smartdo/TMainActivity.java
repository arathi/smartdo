package com.smartdo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class TMainActivity extends Activity {

	Button save;
	Button cancel;
	Button trigger;
	Button action;
	EditText title;
	AutomationItem ai = new AutomationItem();
	Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tmain);

		
		
		save=(Button) findViewById(R.id.buttonSave);
		cancel=(Button) findViewById(R.id.buttonCancel);

		title=(EditText) findViewById(R.id.auto_title_editText);



		save.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view){
				ai.title=title.getText().toString();

			}		
		});

		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				ai = null;
				finish();
			}
		});
	}

	public void action(){
		action=(Button) findViewById(R.id.buttonAction);
		action.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub//may contain error
				Intent intent=new Intent(TMainActivity.this,Action.class);
				intent.putExtra("action", "ai");
				startActivityForResult(intent,1125);
			}
		});

	}
	public void trigger(){

		trigger=(Button) findViewById(R.id.buttonTrigger);
		trigger.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				//	 TODO Auto-generated method stub//may contain error
				Intent intent = new Intent(TMainActivity.this,Trigger.class);
				intent.putExtra("trigger", "ai");
				startActivityForResult(intent,1124);
			}
		});

	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == 1125) {

			if (resultCode == 1025) {    
				startActivity(getIntent()); 
			}
		}
		if (requestCode == 1124) {

			if (resultCode == 1024) {    
				startActivity(getIntent()); 
			}
		}

	}//onActivityResult
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_tmain, menu);
		return true;
	}

}
