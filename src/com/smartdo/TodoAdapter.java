/*
 * package com.example.customadapter;

import android.content.Context;
import android.util.*;
import android.view.*;
import android.widget.*;
import java.util.ArrayList;

public class CustomAdapter extends BaseAdapter{
	private final static String TAG=CustomAdapter.class.getSimpleName();
	ArrayList<DataModel> listArray;
	
	public CustomAdapter(){
		listArray=new ArrayList<DataModel>(5);
		listArray.add(new DataModel("name1", 5, 1.8, "Java"));
		listArray.add(new DataModel("name2", 6, 2.8, "C++"));
		listArray.add(new DataModel("name3", 7, 3.8, "C"));
	}
	
	@Override
	public int getCount(){
		return listArray.size();
	}
	
	@Override
	public Object getItem(int i){
		return listArray.get(i);
	}
	
	@Override
	public long getItemId(int i){
		return i;
	}
	
	@Override
	public View getView(int index, View view, final ViewGroup parent){
		if(view==null){
			LayoutInflater inflater=LayoutInflater.from(parent.getContext());
			view = inflater.inflate(R.layout.single_list_item, parent, false);
		}
		
		final DataModel dataModel=listArray.get(index);
		
		TextView textView=(TextView) view.findViewById(R.id.tv_string_data);
		textView.setText(dataModel.getName());
		
		Button button=(Button) view.findViewById(R.id.btn_number_data);
		button.setText(""+dataModel.getAnInt());
		
		textView=(TextView) view.findViewById(R.id.tv_double_data);
		textView.setText(""+dataModel.getaDouble());
		
		button.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Log.d(TAG, "string: "+ dataModel.getName());
				Log.d(TAG, "int: "+ dataModel.getAnInt());
				Log.d(TAG, "double: "+ dataModel.getaDouble());
				Log.d(TAG, "otherData: "+ dataModel.getOtherData());
				Toast.makeText(parent.getContext(), "button clicked: " + dataModel.getAnInt(), Toast.LENGTH_SHORT).show();
			}
		});
		
		view.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Log.d(TAG, "string: "+ dataModel.getName());
				Log.d(TAG, "int: "+ dataModel.getAnInt());
				Log.d(TAG, "double: "+ dataModel.getaDouble());
				Log.d(TAG, "otherData: "+ dataModel.getOtherData());
				Toast.makeText(parent.getContext(), "view clicked: " + dataModel.getAnInt(), Toast.LENGTH_SHORT).show();
				
			}
		});
		
		return view;
	}
}

 * */

package com.smartdo;

import java.util.Collections;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.smartdo.files.TodoFileProcessor;
/**
 * 
 * @author Amrit
 *This class is a custom adapter which fills the ListView with TodoItems and check boxes
 */
public class TodoAdapter  extends BaseAdapter{
	Context context;
	TextView textView;
	CheckBox checkBox;
	TodoAdapter thisOne;
	
	public TodoAdapter(Context context){
		thisOne=this;
		this.context=context;
	}
	/**
	 * @author Amrit
	 * This method sets the title of the todo and puts a check box in each row of the ListView
	 */
	@Override
	public View getView(final int index, View view, final ViewGroup parent){
		
		if(view==null){
			LayoutInflater inflater=LayoutInflater.from(parent.getContext());
			view=inflater.inflate(R.layout.single_todo_item, parent, false);
		}
		
		
		textView=(TextView) view.findViewById(R.id.todo_title1);
		TodoItem temp=(TodoItem) getItem(index);
		textView.setText(temp.title);
		
		checkBox=(CheckBox) view.findViewById(R.id.todo_complete);
		//checkBox.setChecked(false);
	
		checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
				if(isChecked){
					final TodoItem item=(TodoItem)getItem(index);
					AlertDialog.Builder alert=new AlertDialog.Builder(context);
					alert.setTitle("Done?");
					alert
						.setMessage("Task Completed? It will be deleted.")
						.setCancelable(true)
						.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int arg1) {
								
								
								/**
								 * check if author is me otherwise send to author. 
								 */
								
								
//								item.author="8878874546";
								
								/**
								 * 
								 */
								
								
								Log.i("Todo Done", "delete todo");
								Log.i("Todo Delete", ""+item.author);
								if(!(item.author.contains("me"))){
									Log.i("Sending done SMS", "ASDSAD");
									SmsMgr.SMSSendTaskDone(context, item);
								}
								TodoFileProcessor.deleteTodo(context, item);
								update();
								dialog.dismiss();
							}
						})
					.setNegativeButton("No", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
							checkBox.setChecked(false);
						}
					});
					AlertDialog d= alert.create();
					d.show();
				}
			}
		});
		return view;
	}

	@Override
	public int getCount() {
		return TodoFileProcessor.todoItems.size();
	}

	@Override
	public Object getItem(int i) {
		return TodoFileProcessor.todoItems.get(i);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}
	
	public void update(){
		notifyDataSetChanged();
	}
	
	@Override
	public void notifyDataSetChanged(){
		Collections.sort(TodoFileProcessor.todoItems);
		super.notifyDataSetChanged();
	}
	
}
