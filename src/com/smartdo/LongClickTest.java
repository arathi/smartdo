package com.smartdo;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;
import android.view.Menu;
import android.view.MotionEvent;
import android.widget.TextView;

public class LongClickTest extends Activity implements OnGestureListener, OnDoubleTapListener {
	TextView h;
	final Context context = this;
    private GestureDetector gestureScanner;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_long_click_test);
		h = (TextView) findViewById(R.id.longclick);
		h.setClickable(true);
		gestureScanner=new GestureDetector(this);
		
		gestureScanner.setOnDoubleTapListener(new OnDoubleTapListener() {
			
			@Override
			public boolean onSingleTapConfirmed(MotionEvent e) {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public boolean onDoubleTapEvent(MotionEvent e) {
				h.setText("yo");
				Log.d("onDoubleTapEvent", "yo!");
				return false;
			}
			
			@Override
			public boolean onDoubleTap(MotionEvent e) {
				h.setText("yo");
				Log.d("onDoubleTap", "yo!");
				return false;
			}
		});
//		h.setOnTouchListener(new OnTouchListener() {
//
//			@Override
//			public boolean onTouch(View arg0, MotionEvent arg1) {
//				if (arg1.getAction() == MotionEvent.ACTION_DOWN) {
//					final Timer longpress = new Timer();
//					longpress.schedule(new TimerTask() {
//						@Override
//						public void run() {
//							AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
//									context);
//
//							// set title
//							alertDialogBuilder.setTitle("Your Title");
//
//							// set dialog message
//							alertDialogBuilder
//									.setMessage("Click yes to exit!")
//									.setCancelable(false)
//									.setPositiveButton(
//											"Yes",
//											new DialogInterface.OnClickListener() {
//												public void onClick(
//														DialogInterface dialog,
//														int id) {
//													dialog.cancel();
//												}
//											})
//									.setNegativeButton(
//											"No",
//											new DialogInterface.OnClickListener() {
//												public void onClick(
//														DialogInterface dialog,
//														int id) {
//													// if this button is
//													// clicked, just close
//													// the dialog box and do
//													// nothing
//													dialog.cancel();
//												}
//											});
//
//							// create alert dialog
//							AlertDialog alertDialog = alertDialogBuilder
//									.create();
//
//							// show it
//							alertDialog.show();
//						}
//					}, 2000);
//				}
//				return false;
//			}
//		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_long_click_test, menu);
		return true;
	}

	public void changeText() {
		h.setText("long click!");
	}

	@Override  
    public boolean onTouchEvent(MotionEvent event)  
    {  
        return gestureScanner.onTouchEvent(event);//return the double tap events  
    }

	@Override
	public boolean onSingleTapConfirmed(MotionEvent arg0) {
		// TODO Auto-generated method stub
		return false;
	}

		@Override
	public boolean onDown(MotionEvent arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onFling(MotionEvent arg0, MotionEvent arg1, float arg2,
			float arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onLongPress(MotionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onScroll(MotionEvent arg0, MotionEvent arg1, float arg2,
			float arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onShowPress(MotionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onSingleTapUp(MotionEvent arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onDoubleTap(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}
}
