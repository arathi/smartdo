package com.smartdo;



import com.smartdo.files.TodoFileProcessor;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
/**
 * The activity displayed when todoitem alert in notification bar is clicked.
 * @author Aref
 *
 */
public class AlertAct extends Activity {

	TodoItem todo = new TodoItem();
	int rcode;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_alert);
		rcode = getIntent().getIntExtra("rcode", 0);
		TextView t = (TextView) findViewById(R.id.textView1);
		String s = "";
		Intent i = getIntent();
		s=s+"SMARTDO ALERT\n";
		Bundle b = i.getExtras();
		
		String key = LocationManager.KEY_PROXIMITY_ENTERING;
        Boolean entering = i.getBooleanExtra(key,  false);
         
		//Log.i("ALERT TYPE", b.getString("Type"));
		 todo = (TodoItem) b.getSerializable("Todo");
		s=s+"Alert Type: "+ i.getStringExtra("Type")+"\n"+"You have to "+todo.getTitle()+"\nAs instructed by "+todo.getAuthor()+"\nEntering:"+entering;
		t.setText(s);
	}
	
	public void finish(View view){
		NotificationManager nm;
		nm=(NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
		if(rcode==0){
			nm.cancelAll();
		}
		else{
			nm.cancel(rcode);
		}
		super.finish();
		
	}
	/**
	 * This method calls SmartDo main activity
	 * @author Aref
	 * @param view
	 */
	public void launchApp(View view){
		Intent i = new Intent(this, MainActivity.class);
		startActivity(i);
		finish(null);
	}
	
//	//RENAME TO TODO DONE
//	public void removeTodo(View view){
//		TodoFileProcessor.deleteTodo(getApplicationContext(), todo);
//		finish(null);
//	}

}
