package com.smartdo;

import java.io.Serializable;
import java.util.Date;
/**
 * 
 * @author Amrit
 * This class is the TodoItem which holds information related to a single Todo
 * It implements Serializable, Comparable<TodoItem> interfaces to store in file
 * and to sort the TodoItems by date
 */
@SuppressWarnings("serial")
public class TodoItem implements Serializable, Comparable<TodoItem>{
	public TodoItem(String title){
		this.title=title;
	}
	public TodoItem(){
		
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getSno1() {
		return sno1;
	}
	public void setSno1(int sno1) {
		this.sno1 = sno1;
	}
	public int getSno2() {
		return sno2;
	}
	public void setSno2(int sno2) {
		this.sno2 = sno2;
	}
	public Date getDue_date() {
		return due_date;
	}
	public void setDue_date(Date due_date) {
		this.due_date = due_date;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getAssigned_to() {
		return assigned_to;
	}
	public void setAssigned_to(String assigned_to) {
		this.assigned_to = assigned_to;
	}
	public float getRadius() {
		return radius;
	}
	public void setRadius(float radius) {
		this.radius = radius;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public boolean isConfirmed() {
		return isConfirmed;
	}
	public void setConfirmed(boolean isConfirmed) {
		this.isConfirmed = isConfirmed;
	}
	public boolean isDone() {
		return isDone;
	}
	public void setDone(boolean isDone) {
		this.isDone = isDone;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String title;
	public int sno1;
	public int sno2;
	public Date due_date;
	public double longitude;
	public double latitude;
	public String author;
	public String assigned_to;
	public float radius;
	public int priority;
	public boolean isConfirmed;
	public boolean isDone;
	public String mobileNo;
	public boolean justAlerted=false;
	@Override
	/**
	 * @author Amrit
	 * This method compares the two TodoItems on the basis of their dates.
	 */
	public int compareTo(TodoItem item) {
		 if (getDue_date() == null || item.getDue_date() == null)
			 return 0;
		return getDue_date().compareTo(item.getDue_date());
	}
}