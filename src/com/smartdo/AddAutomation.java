//package com.smartdo;
//
//import java.util.Calendar;
//
//import android.annotation.SuppressLint;
//import android.app.Activity;
//import android.app.DatePickerDialog;
//import android.app.Dialog;
//import android.app.TimePickerDialog;
//import android.os.Bundle;
//import android.support.v4.app.NavUtils;
//import android.view.Menu;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.widget.Button;
//import android.widget.DatePicker;
//import android.widget.EditText;
//import android.widget.RadioButton;
//import android.widget.TextView;
//import android.widget.TimePicker;
//
//import com.smartdo.files.AutoFileProcessor;
//
//@SuppressLint("NewApi")
//public class AddAutomation extends Activity {
//
//	int mYear, mMonth, mDay;
//	int mHour, mMinute;
//	
//	Button save;
//	Button cancel;
//	TextView timeView;
//	TextView dateView;
//	EditText title;
//	AutomationItem temp;
//	TextView autoLocation;
//	RadioButton radioButton_wifi;
//	RadioButton radioButton_GPS;
//	RadioButton radioButton_GPRS;
//	RadioButton radioButton_silent;
//	
//	@SuppressLint("NewApi")
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_add_automation);
//		// Show the Up button in the action bar.
//		//getActionBar().setDisplayHomeAsUpEnabled(true);
//		timeView=(TextView)findViewById(R.id.autoTime);
//		dateView=(TextView)findViewById(R.id.autoDate);
//		title=(EditText)findViewById(R.id.auto_title_editText);
//		autoLocation=(TextView)findViewById(R.id.autoLocation);
//		radioButton_wifi=(RadioButton) findViewById(R.id.radioButton_wifi);
//		radioButton_GPS=(RadioButton) findViewById(R.id.radioButton_GPS);
//		radioButton_GPRS=(RadioButton) findViewById(R.id.radioButton_GPRS);
//		radioButton_silent=(RadioButton) findViewById(R.id.radioButton_silent);
//		
//		radioButton_wifi.isChecked();
//		
//		//what to do
//		
//		final Calendar c=Calendar.getInstance();
//		mYear=c.get(Calendar.YEAR);
//		mMonth=c.get(Calendar.MONTH);
//		mDay=c.get(Calendar.DAY_OF_MONTH);
//		mHour=c.get(Calendar.HOUR_OF_DAY);
//		mMinute=c.get(Calendar.MINUTE);
//
//		save=(Button)findViewById(R.id.saveButtonAddauto);
//		cancel=(Button)findViewById(R.id.cancelButtonAddauto);
//		
//		save.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View view) {
//				temp=new AutomationItem();
//				temp.title = title.getText().toString();
////				try {
////					Date date1=new SimpleDateFormat("dd-MM-yyyy HH:mm").parse(dateView.getText().toString()+timeView.getText().toString());
////				//	temp.setDue_date(date1);
////				} catch (ParseException e) {e.printStackTrace();} 
//				writeAuto(temp);
//				
//				finish();
//			}
//		});
//		cancel.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View view) {
//				temp=null;
//				finish();
//			}
//		});
//	}
//
//	public void writeAuto(AutomationItem auto){
//		AutoFileProcessor.writeAuto(this, auto);
//	}
//	
//	@Override
//	public void finish(){
//		if(temp==null){
//			setResult(RESULT_CANCELED);
//		}
//		else{
//			setResult(RESULT_OK);
//			super.finish();
//		}
//	}
//
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.activity_add_automation, menu);
//		return true;
//	}
//
//
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		switch (item.getItemId()) {
//		case android.R.id.home:
//			// This ID represents the Home or Up button. In the case of this
//			// activity, the Up button is shown. Use NavUtils to allow users
//			// to navigate up one level in the application structure. For
//			// more details, see the Navigation pattern on Android Design:
//			//
//			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
//			//
//			NavUtils.navigateUpFromSameTask(this);
//			return true;
//		}
//		return super.onOptionsItemSelected(item);
//	}
//	@SuppressWarnings("deprecation")
//	public void showDatePicker(View view){
//		showDialog(0);
//	}
//	
//	@SuppressWarnings("deprecation")
//	public void showTimePicker(View view){
//		showDialog(999);
//	}
//	
//	private DatePickerDialog.OnDateSetListener mDateSetListner=new DatePickerDialog.OnDateSetListener() {
//		
//		@Override
//		public void onDateSet(DatePicker view, int year, int monthOfYear,
//				int dayOfMonth) {
//			mDay=dayOfMonth;
//			mYear=year;
//			mMonth=monthOfYear;
//			if(monthOfYear==0){
//				monthOfYear=1;
//			}
//			TextView dateView=(TextView)findViewById(R.id.autoDate);
//			dateView.setText(dayOfMonth +"/"+ monthOfYear+"/"+year);
//		}
//	};
//	
//	private TimePickerDialog.OnTimeSetListener mTimeSetListener=new TimePickerDialog.OnTimeSetListener() {
//		
//		@Override
//		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//			mHour=hourOfDay;
//			mMinute=minute;
//			TextView timeView=(TextView)findViewById(R.id.autoTime);
//			timeView.setText(" "+hourOfDay+ ":" +minute);
//		}
//	};
//	
//	@Override
//	protected Dialog onCreateDialog(int id){
//		switch(id){
//		case 0:
//			return new DatePickerDialog(this, mDateSetListner, mYear, mMonth, mDay);
//		case 999:
//			return new TimePickerDialog(this, mTimeSetListener, mHour, mMinute, false);
//		}
//		return null;
//	}
//
//}

package com.smartdo;

import java.util.Random;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.smartdo.files.AutoFileProcessor;
/**
 * @author Ashish
 * This class adds an Automation task
 * based on user selected Actions and Triggers
 *
 */
@SuppressLint("NewApi")
public class AddAutomation extends Activity {

	Button save;
	Button cancel;
	Button trigger;
	Button action;
	EditText title;
	AutomationItem ai;
	Context context;
	String titlevalue="";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tmain);

		ai = new AutomationItem();

		save=(Button) findViewById(R.id.buttonSave);
		cancel=(Button) findViewById(R.id.buttonCancel);

		title=(EditText) findViewById(R.id.auto_title_editText);

		action=(Button) findViewById(R.id.buttonAction);
		trigger=(Button) findViewById(R.id.buttonTrigger);

		
		
		
		trigger.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(AddAutomation.this,Trigger.class);
				intent.putExtra("trigger", ai);
				startActivityForResult(intent,1124);	
			}
		});

		
		
		action.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent=new Intent(AddAutomation.this,Action.class);
				intent.putExtra("action", ai);
			//	Toast.makeText(getApplicationContext(), "You made action mess", Toast.LENGTH_LONG).show();
				startActivityForResult(intent,1125);
			}
		});

		save.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view){
				Random gen = new Random(); 
				ai.title=title.getText().toString();
				//Toast.makeText(getApplicationContext(),ai.title +" "+ai.triggers.size()+ai.actions.size() , Toast.LENGTH_LONG).show();
				ai.sno1 = gen.nextInt(1000);
				ai.sno2 = gen.nextInt(1000);
				
				writeAuto(ai);
				AutomationHandler.addAutomationTask(getApplicationContext(), ai);
				Log.i("AddAutomation","AUTOMATION ITEM SAVED");
				AutomationHandler.printAutomationTask(ai);		//print all to LogCat
				finish();
			}		
		});

		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				ai = null;
				finish();
			}
		});
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	if(!titlevalue.equals(""))
	{
		title.setText(titlevalue);
	}
	}
	
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		titlevalue=title.getText().toString();
	}
	public void writeAuto(AutomationItem auto) {
		AutoFileProcessor.writeAuto(this, auto);
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if (requestCode == 1124 && resultCode == 1024) {    
			//back from triggers
			onResume();
			ai=(AutomationItem) data.getSerializableExtra("trigger");
			trigger.setText("Added");
			//startActivity(getIntent());
			
		}
		if (requestCode == 1125 && resultCode == 1025) {    
			onResume();
			ai=(AutomationItem) data.getSerializableExtra("action");
			action.setText("Added");
			//startActivity(getIntent()); 
		}

	}//onActivityResult
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_tmain, menu);
		return true;
	}
}
