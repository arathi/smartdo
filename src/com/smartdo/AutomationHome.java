//package com.smartdo;
//
//import java.io.File;
//
//import android.annotation.SuppressLint;
//import android.app.Activity;
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v4.app.NavUtils;
//import android.view.ContextMenu;
//import android.view.ContextMenu.ContextMenuInfo;
//import android.view.Menu;
//import android.view.MenuInflater;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.widget.Button;
//import android.widget.ListView;
//
//import com.smartdo.files.AutoFileProcessor;
//
//@SuppressLint("NewApi")
//public class Automation_home extends Activity {
//	public AutoAdapter adapter;
//	public static File todoFile;
//	public static File autoFile;
//	ListView autoList;
//	Button todo;
//	@SuppressLint("NewApi")
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		
//			super.onCreate(savedInstanceState);
//			setContentView(R.layout.activity_automation_home);
//			// Show the Up button in the action bar.
//			AutoFileProcessor.getAutos(this);
//			autoList=(ListView) findViewById(R.id.automationList);
//			adapter=new AutoAdapter();
//			autoList.setAdapter(adapter);
//			todo=(Button)findViewById(R.id.button1);
//			todo.setOnClickListener(new OnClickListener() {
//				
//				@Override
//				public void onClick(View view) {
//					finish();
//				}
//			});
//		}
//			
//			/*
//			ArrayList<String> autoList=new ArrayList<String>();
//			ArrayAdapter<String> adapter=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, autoList);
//			list=(ListView)findViewById(R.id.automationList);
//			
//			getActionBar().setDisplayHomeAsUpEnabled(true);
//			autoList.add("item1");
//			autoList.add("item2");
//			autoList.add("item3");
//			list.setAdapter(adapter);
//			adapter.notifyDataSetChanged();
//			}catch(Exception e){Log.d("onCreate",e.toString() );}
//*/
//	
//
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.activity_automation_home, menu);
//		return true;
//	}
//
//	public boolean onOptionsItemSelected(MenuItem item) {
//		switch (item.getItemId()) {
//		case android.R.id.home:
//			// This ID represents the Home or Up button. In the case of this
//			// activity, the Up button is shown. Use NavUtils to allow users
//			// to navigate up one level in the application structure. For
//			// more details, see the Navigation pattern on Android Design:
//			//
//			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
//			//
//			NavUtils.navigateUpFromSameTask(this);
//			
//			return true;
//			
//		case R.id.add_auto_option:
//	//		Intent intent=new Intent(this,AddAutomation.class);
//		//	startActivity(intent);
//			viewAddAuto();
//			return true;
//		}
//		return super.onOptionsItemSelected(item);
//	}
//	
//	public void viewAddAuto(){
//		Intent intent=new Intent(this, AddAutomation.class);
//		startActivityForResult(intent, 1);
//	}
//	
//	@Override
//	protected void onActivityResult(int requestCode, int resultCode, Intent data){
//		if(resultCode==RESULT_OK){
//			adapter.notifyDataSetChanged();
//		}
//		else if(resultCode==RESULT_CANCELED){
//			adapter.notifyDataSetChanged();
//		}
//	}
//	public void onCreateContextMenu(ContextMenu menu, View v,
//            ContextMenuInfo menuInfo) {
//			super.onCreateContextMenu(menu, v, menuInfo);
//			MenuInflater inflater = getMenuInflater();
//		//	inflater.inflate(R.menu., menu);
//	}
//	
//}

package com.smartdo;

import java.io.File;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.smartdo.files.AutoFileProcessor;

/**
 * @author Ashish
 *
 */
public class AutomationHome extends Activity {

	public AutoAdapter adapter;
	
	private GestureDetector gestureDetector;

	
	public static File autoFile;
	ListView autoList;
	ArrayList<AutomationItem> autos;
	Context context=this;
	String flingdir = "";
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
			super.onCreate(savedInstanceState);
			
			String anim = ""+getIntent().getStringExtra("anim");
			
			if(anim.equals("left")){
				flingdir="left";
				overridePendingTransition(R.anim.rightflingin,R.anim.rightflingout);
			}
			else if (anim.equals("right")){
				flingdir="right";
				overridePendingTransition(R.anim.leftflingin,R.anim.leftflingout);
			}
			
			gestureDetector = new GestureDetector(
	                new SimpleOnGestureListener(){
	                	 private static final int SWIPE_MIN_DISTANCE = 120;
	                	    private static final int SWIPE_MAX_OFF_PATH = 200;
	                	    private static final int SWIPE_THRESHOLD_VELOCITY = 200;

	                	    @Override
	                	    public boolean onFling(MotionEvent e1, MotionEvent e2,
	                	                         float velocityX, float velocityY) {
	                	      try {
	                	        float diffAbs = Math.abs(e1.getY() - e2.getY());
	                	        float diff = e1.getX() - e2.getX();

	                	        if (diffAbs > SWIPE_MAX_OFF_PATH)
	                	          return false;
	                	        
	                	        // Left swipe
	                	        if (diff > SWIPE_MIN_DISTANCE
	                	        && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
		                 	           showMap(null);


	                	        // Right swipe
	                	        } else if (-diff > SWIPE_MIN_DISTANCE
	                	        && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
	                	        	showTodo(null);

	                	        }
	                	      } catch (Exception e) {
	                	        Log.e("MainActivity", "Error on gestures");
	                	      }
	                	      return false;
	                	    }
	                	  }
	                );
			
			
			
			setContentView(R.layout.activity_automation_home);
			// Show the Up button in the action bar.
			AutoFileProcessor.getAutos(this);
			
			autoList=(ListView) findViewById(R.id.automationList);
			adapter=new AutoAdapter(context);
			autoList.setAdapter(adapter);
			autos=AutoFileProcessor.getAutos(context);
		}
	
	public void automationDetails(View view){
		Intent intent=new Intent (this, AutomationDetails.class);
		TextView title=(TextView)view;
		String title1=(String) title.getText();
		AutomationItem ai=null;
		for(int i=0;i<AutoFileProcessor.automationItems.size();i++){
			if(AutoFileProcessor.automationItems.get(i).title.equals(title1)){
				ai=AutoFileProcessor.automationItems.get(i);				
			}
		}
		intent.putExtra("auto", ai);
		startActivity(intent);
	}

	@Override
	  public boolean onTouchEvent(MotionEvent event) {
		Log.i("motion", "yup");
	    if (gestureDetector.onTouchEvent(event)) {
	      return true;
	    }
	    return super.onTouchEvent(event);
	  }
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_automation_home, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			
			return true;
			
		case R.id.add_auto_option:
	//		Intent intent=new Intent(this,AddAutomation.class);
		//	startActivity(intent);
			viewAddAuto();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void viewAddAuto(){
		Intent intent=new Intent(this, AddAutomation.class);
		startActivityForResult(intent, 1);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		if(resultCode==RESULT_OK){
			adapter.notifyDataSetChanged();
		}
		else if(resultCode==RESULT_CANCELED){
			adapter.notifyDataSetChanged();
		}
	}
	
	public void showMap(View view){
		Intent intent=new Intent(this, MapProcessor.class);
		intent.putExtra("action", "overlays");
		intent.putExtra("anim","right");
		intent.putExtra("caller","auto");
		startActivityForResult(intent, 2001);
	}
	
	public void showTodo(View view){
		finish();
	}
	  
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
		//this.finish();
		moveTaskToBack(true);
	}
	
	@Override
	public void finish() {
		super.finish();
		if(flingdir.equals("left"))
			overridePendingTransition(R.anim.leftflingin,R.anim.leftflingout);
		else if (flingdir.equals("right"))
			overridePendingTransition(R.anim.rightflingin, R.anim.rightflingout);
		else
			overridePendingTransition(0,0);

	}
}

