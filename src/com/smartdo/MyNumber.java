package com.smartdo;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.smartdo.files.MyNumberFileProcessor;

public class MyNumber extends Activity {

	EditText number;
	Button save, cancel;
	Context context;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context=this;
		setContentView(R.layout.activity_my_number);
		number=(EditText)findViewById(R.id.my_mobile_number);
		save=(Button)findViewById(R.id.save_number);
		cancel=(Button)findViewById(R.id.cancel_number);
		
		save.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				String myNumber=number.getText().toString();
				MyNumberFileProcessor.updateMobileNumber(context, myNumber);
				finish();
			}
		});
		
		cancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_my_nymber, menu);
		return true;
	}

}
