package com.smartdo;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.ads.b;
import com.smartdo.files.AutoFileProcessor;

@SuppressLint("NewApi")
/**
 * @author Ashish
 * This class takes all the actions 
 * that are to be performed in an
 * Automation
 * 
 *
 */
public class Action extends Activity {

	TextView autoContact;
	Context context=this;
	AutomationItem ai;
	
	Button ok;
	
	CheckBox ringer;
	CheckBox bluetooth;
	CheckBox wifi;
	CheckBox data;
	CheckBox brightness;
	CheckBox SMS;
	
	EditText SMStext;
	String con;
	RadioGroup ringergp;
	RadioGroup bluetoothgp;
	RadioGroup wifigp;
	RadioGroup datagp;
	RadioGroup brightgp;
	
	RadioButton ringer1;
	RadioButton ringer2;
	RadioButton ringer3;
	RadioButton bluetooth1;
	RadioButton bluetooth2;
	RadioButton data1;
	RadioButton data2;
	RadioButton wifi1;
	RadioButton wifi2;
	RadioButton brightness1;
	RadioButton brightness2;
	
	int ringerflag1=0,ringerflag2=0,ringerflag3=0;
	int bluetoothflag1=0,bluetoothflag2=0;
	int dataflag1=0,dataflag2=0;
	int wififlag1=0,wififlag2=0;
	int brightflag1=0,brightflag2=0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_action);
		
		ai=(AutomationItem) getIntent().getSerializableExtra("action");
		
		if(!ai.actions.isEmpty()){
			if(ai.actions.get(AutomationItem.RINGER_MODE_ACTION)!=null){
				ringer=(CheckBox) findViewById(R.id.checkBoxRinger1);
				ringer.setChecked(true);
				switch (Integer.parseInt(ai.actions.get(AutomationItem.RINGER_MODE_ACTION))) {
					case 0:
						ringer1=(RadioButton) findViewById(R.id.radioButtonRinger1);
						ringer1.setChecked(true);
						break;
	
					case 1:
						ringer2=(RadioButton) findViewById(R.id.radioButtonRinger2);
						ringer2.setChecked(true);
						break;
					
					case 2:
						ringer3=(RadioButton) findViewById(R.id.radioButtonRinger3);
						ringer3.setChecked(true);
						break;
				}
			}
			if(ai.actions.get(AutomationItem.BLUETOOTH_ACTION)!=null){
				bluetooth=(CheckBox) findViewById(R.id.checkBoxBluetooth);
				bluetooth.setChecked(true);
				String string=ai.actions.get(AutomationItem.BLUETOOTH_ACTION);
				if(string.equals(""+AutomationItem.TURN_OFF)){
					bluetooth2=(RadioButton) findViewById(R.id.radioButtonBluetooth2);
					bluetooth2.setChecked(true);
					
				}
				else{
					bluetooth1=(RadioButton) findViewById(R.id.radioButtonBluetooth1);
					bluetooth1.setChecked(true);
				}				
			}
			if(ai.actions.get(AutomationItem.DATA_ACTION)!=null){
				data=(CheckBox) findViewById(R.id.checkBoxData);
				data.setChecked(true);
				String string=ai.actions.get(AutomationItem.DATA_ACTION);
				if(string.equals(""+AutomationItem.TURN_OFF)){
					data2=(RadioButton) findViewById(R.id.radioButtonData2);
					data2.setChecked(true);
				}else{
					data1=(RadioButton) findViewById(R.id.radioButtonData1);
					data1.setChecked(true);
				}
			}
			if(ai.actions.get(AutomationItem.WIFI_ACTION)!=null){
				wifi=(CheckBox) findViewById(R.id.checkBoxWifi);
				wifi.setChecked(true);
				String string=ai.actions.get(AutomationItem.WIFI_ACTION);
				if(string.equals(""+AutomationItem.TURN_OFF)){
					wifi2=(RadioButton) findViewById(R.id.radioButtonWifi2);
					wifi2.setChecked(true);
				}else{
					wifi1=(RadioButton) findViewById(R.id.radioButtonWifi1);
					wifi1.setChecked(true);
				}
			}
			if(ai.actions.get(AutomationItem.SCREEN_BRIGHTNESS_ACTION)!=null){
				brightness=(CheckBox) findViewById(R.id.checkBoxBrightness);
				brightness.setChecked(true);
				
			}
			if(ai.actions.get(AutomationItem.SMS_ACTION)!=null){
				SMS=(CheckBox) findViewById(R.id.checkBoxSMS);
				SMS.setChecked(true);
				String string=ai.actions.get(AutomationItem.SMS_ACTION);
				String[] str = string.split(",");
				autoContact=(TextView) findViewById(R.id.textViewContact);
				autoContact.setText(str[0]);
				SMStext=(EditText) findViewById(R.id.editTextSMS);
				SMStext.setText(str[1]);
				
			}
		}
		
		ringergp=(RadioGroup) findViewById(R.id.radioGroupRinger);
		ringer1=(RadioButton) findViewById(R.id.radioButtonRinger1);
		ringer2=(RadioButton) findViewById(R.id.radioButtonRinger2);
		ringer3=(RadioButton) findViewById(R.id.radioButtonRinger3);

		bluetoothgp=(RadioGroup) findViewById(R.id.radioGroupBluetooth);
		bluetooth1=(RadioButton) findViewById(R.id.radioButtonBluetooth1);
		bluetooth2=(RadioButton) findViewById(R.id.radioButtonBluetooth2);

		datagp=(RadioGroup) findViewById(R.id.radioGroupData);
		data1=(RadioButton) findViewById(R.id.radioButtonData1);
		data2=(RadioButton) findViewById(R.id.radioButtonData2);

		wifigp=(RadioGroup) findViewById(R.id.radioGroupWifi);
		wifi1=(RadioButton) findViewById(R.id.radioButtonWifi1);
		wifi2=(RadioButton) findViewById(R.id.radioButtonWifi2);
		
		brightgp=(RadioGroup) findViewById(R.id.radioGroupBrightness);
		brightness1=(RadioButton) findViewById(R.id.radioButtonBrightness1);
		brightness2=(RadioButton) findViewById(R.id.radioButtonBrightness2);
		autoContact=(TextView) findViewById(R.id.textViewContact);

		SMStext=(EditText) findViewById(R.id.editTextSMS);
	
		//ringergp.setActivated(false);
		//ringergp.setVisibility(DEFAULT_KEYS_DISABLE);
		
		//changeOption(ringergp);
		
		ringer1.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(ringer1.isChecked()){
					ringerflag1=1;					
				}
			}
			
		});
		
		ringer2.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(ringer2.isChecked()){
					ringerflag2=1;
				}
			}
		});
		ringer3.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(ringer3.isChecked()){
					ringerflag3=1;
				}
			}
		});	


		bluetooth1.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(bluetooth1.isChecked()){
					bluetoothflag1=1;
				}
			}
		});
		bluetooth2.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(bluetooth2.isChecked()){
					bluetoothflag2=1;
				}
				
			}
		});

		data1.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(data1.isChecked()){
					dataflag1=1;
				}		
			}
		});
		data2.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(data2.isChecked()){
					dataflag2=1;
				}		
			}
		});
	
		wifi1.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(wifi1.isChecked()){
					wififlag1=1;
				}		
			}
		});
		wifi2.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(wifi2.isChecked()){
					wififlag2=1;
				}		
			}
		});
		
		Button ok=(Button) findViewById(R.id.buttonOK);
		ok.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				
				doSave();
			}
		});
	}
	
	void changeOption(RadioGroup radiogp){
		for(int i=0;i<radiogp.getChildCount();i++){
			((RadioButton)radiogp.getChildAt(i)).setEnabled(false);
		}
	}
	
	void changeOptionClick(RadioGroup radiogp){
		for(int i=0;i<radiogp.getChildCount();i++){
			((RadioButton)radiogp.getChildAt(i)).setEnabled(true);
		}
	}

	void doSave(){

		ringer=(CheckBox) findViewById(R.id.checkBoxRinger1);
		bluetooth=(CheckBox) findViewById(R.id.checkBoxBluetooth);
		data=(CheckBox) findViewById(R.id.checkBoxData);
		wifi=(CheckBox) findViewById(R.id.checkBoxWifi);
		brightness=(CheckBox) findViewById(R.id.checkBoxBrightness);
		SMS=(CheckBox) findViewById(R.id.checkBoxSMS);

		
		

		if(ringer.isChecked()){
//			for(int i=0;i<ringergp.getChildCount();i++){
//				((RadioButton)ringergp.getChildAt(i)).setEnabled(true);
//			}
//			
			if(ringerflag1==1){
				ai.actions.put(AutomationItem.RINGER_MODE_ACTION,""+AutomationItem.RINGER_SILENT);
			//	ai.actiontext="Ringer Mode:Silent"+"\n";
				Log.i("Ringer", ""+ai.actions.get(AutomationItem.RINGER_MODE_ACTION));
			}
			if(ringerflag2==1){
				ai.actions.put(AutomationItem.RINGER_MODE_ACTION,""+AutomationItem.RINGER_NORMAL);
			//	ai.actiontext="Ringer Mode:Normal"+"\n";
				Log.i("Ringer", ""+ai.actions.get(AutomationItem.RINGER_MODE_ACTION));
			}
			if(ringerflag3==1){
				ai.actions.put(AutomationItem.RINGER_MODE_ACTION,""+AutomationItem.RINGER_VIBRATE);
			//	ai.actiontext="Ringer Mode:Vibrate"+"\n";
				Log.i("Ringer", ""+ai.actions.get(AutomationItem.RINGER_MODE_ACTION));
			}
		}
			

			if(bluetooth.isChecked()){
				if(bluetoothflag1==1){
					ai.actions.put(AutomationItem.BLUETOOTH_ACTION, ""+AutomationItem.TURN_ON);
				//	ai.actiontext=ai.actiontext+" "+"Bluetooth:ON"+"\n";
					Log.i("Bluetooth", ""+ai.actions.get(AutomationItem.BLUETOOTH_ACTION));
				}
				if(bluetoothflag2==1){
					ai.actions.put(AutomationItem.BLUETOOTH_ACTION, ""+AutomationItem.TURN_OFF);
					Log.i("Bluetooth", ""+ai.actions.get(AutomationItem.BLUETOOTH_ACTION));
				}
				
			}

			if(data.isChecked()){
				if(dataflag1==1){
					ai.actions.put(AutomationItem.DATA_ACTION,""+AutomationItem.TURN_ON);
				//	ai.actiontext=ai.actiontext+" "+"Data:ON"+"\n";
					Log.i("Data", ""+ai.actions.get(AutomationItem.DATA_ACTION));
				}
				if(dataflag2==1){
					ai.actions.put(AutomationItem.DATA_ACTION,""+AutomationItem.TURN_OFF);
					Log.i("Data", ""+ai.actions.get(AutomationItem.DATA_ACTION));
				}
			}
			
			if(wifi.isChecked()){
				if(wififlag1==1){
					ai.actions.put(AutomationItem.WIFI_ACTION,""+AutomationItem.TURN_ON);
				//	ai.actiontext=ai.actiontext+" "+"WiFi:ON"+"\n";
					Log.i("WIFI", ""+ai.actions.get(AutomationItem.WIFI_ACTION));
				}
				if(wififlag2==1){
					ai.actions.put(AutomationItem.WIFI_ACTION,""+AutomationItem.TURN_OFF);
					Log.i("WIFI", ""+ai.actions.get(AutomationItem.WIFI_ACTION));
				}
			}
			if(brightness.isChecked()){
				brightness1.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					
					@Override
					public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
						// TODO Auto-generated method stub
						if(brightness1.isChecked()){
							//change the value according to you
							ai.actions.put(AutomationItem.SCREEN_BRIGHTNESS_ACTION,"90");
					//		ai.actiontext=ai.actiontext+" "+"Brightness:Increased"+"\n";
							Log.i("Brightness", ""+ai.actions.get(AutomationItem.SCREEN_BRIGHTNESS_ACTION));

						}
					}
				});
				brightness2.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					
					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						// TODO Auto-generated method stub
						if(brightness2.isChecked()){
							//change the value according to you
							ai.actions.put(AutomationItem.SCREEN_BRIGHTNESS_ACTION,"30");
						//	ai.actiontext=ai.actiontext+" "+"Brightness:Decreased"+"\n";
							Log.i("Brightness", ""+ai.actions.get(AutomationItem.SCREEN_BRIGHTNESS_ACTION));
						}
					}
				});
			}
			if(SMS.isChecked()){
				Log.i("Action", "yo!!43");
				String text=SMStext.getText().toString();
				Log.i("Action", "yo!!");
				ai.actions.put(AutomationItem.SMS_ACTION,con+","+text);
				Log.i("Action", "Inserted SMS Action ");
				Log.i("Action", "Inserted SMS Action "+ ai.actions.get(AutomationItem.SMS_ACTION));
				//ai.actions.put(AutomationItem.SMS_ACTION, ""+text);
			}
			
			finish();
		}

	
	public void contactPicker(View view){
		Intent intent=new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
		startActivityForResult(intent, 1001);  
	}
	
	protected void onActivityResult(int reqcode, int rescode, Intent data){
		super.onActivityResult(reqcode, rescode, data);
		
		if(reqcode==1001 && rescode==RESULT_OK){
			switch(reqcode){
			case 1001:
				Uri result=data.getData();
				int type=ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE;

				String[] whereArgs = new String[] { String.valueOf(result.getLastPathSegment()), String.valueOf(type) };

				   Cursor cursor = getContentResolver().query(
				                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI, 
				                            null,
				                            ContactsContract.CommonDataKinds.Phone._ID + " = ? and " + 
				                            ContactsContract.CommonDataKinds.Phone.TYPE + " = ?", 
				                            whereArgs, 
				                            null);
				   int phoneNumberIndex = cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER);
				   if (cursor != null) {
					   try {
				             if (cursor.moveToFirst()) {
				                 String phoneNumber = cursor.getString(phoneNumberIndex);
				                 //temp.setMobileNo(phoneNumber);
				                 //temp.setAssigned_to(phoneNumber);
				                 //String sms=SMStext.getText().toString();
				                 autoContact.setText(phoneNumber);
				                 con=autoContact.getText().toString();
				                 
				               
				             }
				            } finally {
				               cursor.close();
				            }
				   }

				break;
			}
		}else{
			
			Log.w("contacts picker", "Warning: activity result not ok");
		}
	}
	
		public void writeAuto(AutomationItem auto) {
			AutoFileProcessor.writeAuto(this, auto);
		}

		public void finish(){	
			Intent data=new Intent();
			data.putExtra("action", ai);
			setResult(1025, data);
			super.finish();
		}
		
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.activity_action, menu);
			return true;
		}
		
		protected void onResume(){
			super.onResume();
		}
		
		@Override
		protected void onPause() {
			// TODO Auto-generated method stub
			super.onPause();
		}

	}
