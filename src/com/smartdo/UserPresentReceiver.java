package com.smartdo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;
/**
 * Broadcast receiver for detecting user presence - i.e. unlock screen 
 * 
 * @author aref
 *
 */
public class UserPresentReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.e("HERE","IN USERPRESENTRECEIVER!");
		Toast.makeText(context, "USER PRESENT", Toast.LENGTH_SHORT).show();
		AutomationHandler.doAutoAfterUserPresent(context);	
	}

}
