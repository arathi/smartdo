//package com.smartdo;
//
//import android.app.PendingIntent;
//import android.content.Context;
//import android.content.Intent;
//import android.location.LocationManager;
//import android.util.Log;
//
//
//public class LocaManager {
//
//	public static void addProx(Context c, TodoItem todo){
//		LocationManager locationManager = (LocationManager)c.getSystemService(Context.LOCATION_SERVICE);
//		Intent intent = new Intent("com.spawnalert");
//		intent.putExtra("Todo", todo);
//		intent.putExtra("Type", "GPS"); 	//0 - GPS trigger
//       // PendingIntent proximityIntent = PendingIntent.getActivity(this, 0, intent, 0);
//		PendingIntent proximityIntent = PendingIntent.getBroadcast(c,0, intent, 0);
//		Log.i("YOOO", "Type:"+intent.getExtras().getString("Type"));
//		locationManager.addProximityAlert(todo.getLatitude(), todo.getLongitude(), todo.getRadius(), -1, proximityIntent);
//	}
//	
//	public static void delProx(Context c, TodoItem todo){
//		LocationManager locationManager = (LocationManager)c.getSystemService(Context.LOCATION_SERVICE);
//		Intent intent = new Intent("com.spawnalert");
//		intent.putExtra("Todo", todo);
//		intent.putExtra("Type", "GPS"); 	//0 - GPS trigger
//		PendingIntent proximityIntent = PendingIntent.getBroadcast(c,0, intent, 0);
//		Log.i("DEL", "Type:"+intent.getExtras().getString("Type"));
//		locationManager.removeProximityAlert(proximityIntent);
//
//	}
//	
// 
//	
//}

package com.smartdo;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.util.Log;
/**
 * Location Manager which can addProx alerts using Android SDK. Can delProx too.
 * Discarded. Manual GPS polling adopted due to battery usage of these Android methods.
 * 
 * @author aref
 *
 */
public class LocaManager {

	public static void addProx(Context c, TodoItem todo){
		LocationManager locationManager = (LocationManager)c.getSystemService(Context.LOCATION_SERVICE);
		Intent intent = new Intent("com.spawnalert");
		intent.putExtra("Todo", todo);
		intent.putExtra("Type", "GPS"); 	//0 - GPS trigger
       // PendingIntent proximityIntent = PendingIntent.getActivity(this, 0, intent, 0);
		PendingIntent proximityIntent = PendingIntent.getBroadcast(c,todo.getSno2(), intent, 0);
		Log.i("YOOO", "Type:"+intent.getExtras().getString("Type"));
		locationManager.addProximityAlert(todo.getLatitude(), todo.getLongitude(), todo.getRadius(), -1, proximityIntent);
	}
	
	public static void addProx(Context c, AutomationItem autoI) {
		LocationManager locationManager = (LocationManager)c.getSystemService(Context.LOCATION_SERVICE);
		Intent intent = new Intent("com.autoprocess");
		intent.putExtra("AutomationItem", autoI);
		intent.putExtra("CurrentType", AutomationItem.LOCATION_TRIGGER); 	

		PendingIntent proximityIntent = PendingIntent.getBroadcast(c,autoI.sno2, intent,0);
		String strSplit[] = autoI.triggers.get(AutomationItem.LOCATION_TRIGGER).split(",");
		
		locationManager.addProximityAlert(Double.parseDouble(strSplit[0]), Double.parseDouble(strSplit[1]), Float.parseFloat(strSplit[2]), -1, proximityIntent);
	}
	

	public static void addProx(Context c, AutomationItem autoI,
			Long expiry) {
		LocationManager locationManager = (LocationManager)c.getSystemService(Context.LOCATION_SERVICE);
		Intent intent = new Intent("com.autoprocess");
		intent.putExtra("AutomationItem", autoI);
		intent.putExtra("CurrentType", AutomationItem.LOCATION_TRIGGER); 	

		PendingIntent proximityIntent = PendingIntent.getBroadcast(c,autoI.sno2, intent,0);
		String strSplit[] = autoI.triggers.get(AutomationItem.LOCATION_TRIGGER).split(",");
	
		locationManager.addProximityAlert(Double.parseDouble(strSplit[0]), Double.parseDouble(strSplit[1]), Float.parseFloat(strSplit[2]), expiry, proximityIntent);
	}
	
	public static void delProx(Context c, TodoItem todo){
		LocationManager locationManager = (LocationManager)c.getSystemService(Context.LOCATION_SERVICE);
		Intent intent = new Intent("com.spawnalert");
		intent.putExtra("Todo", todo);
		intent.putExtra("Type", "GPS"); 	//0 - GPS trigger
		PendingIntent proximityIntent = PendingIntent.getBroadcast(c,todo.getSno2(), intent, 0);
		Log.i("DEL", "Type:"+intent.getExtras().getString("Type"));
		locationManager.removeProximityAlert(proximityIntent);

	}


	
	
 
	
}
