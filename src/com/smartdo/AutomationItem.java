package com.smartdo;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;

import android.media.AudioManager;
/**
 * AutomationItem Data Object. Defines constants in Hashmaps of Action and Trigger.
 * 
 * @author aref
 *
 */
public class AutomationItem implements Serializable {
	/**
	 * 
	 * ACTIONS
	 * 
	 */
	public static final int WIFI_ACTION = 1;
	public static final int DATA_ACTION = 2;
	public static final int BLUETOOTH_ACTION = 3;
	/*The values attainable by these actions*/
	public static final int TURN_ON=1;
	public static final int TURN_OFF=0;
	
	public static final int SCREEN_BRIGHTNESS_ACTION = 4;
	/*Brightness level will be set by the int value in HashMap corresponding 
	 * to key value 4*/
	
	public static final int RINGER_MODE_ACTION = 5;
	public static final int RINGER_SILENT=AudioManager.RINGER_MODE_SILENT;
	public static final int RINGER_NORMAL=AudioManager.RINGER_MODE_NORMAL;
	public static final int RINGER_VIBRATE=AudioManager.RINGER_MODE_VIBRATE;
	
	public static final int SMS_ACTION = 6;
	/* The recipient's number + "," + smsBody as value corresponding to the key 6
	 */
	
	/**
	 * 
	 * TRIGGERS
	 * 
	 */
	public static final int LOCATION_TRIGGER=6;	
	/*
	 * Value will be "Latitude, Longitude, Radius"
	 */
	
	public static final int ALARM_TRIGGER=5;
	/*
	 * Value will be Date in Long but String
	 */
	
	public static final int ALARM_TRIGGER_MARGIN = 50;  // NOT DEFAULT VALUE :P but I need the margin in milliseconds
														// convert hours into mseconds. it's another entry in hashmap with key value 50
	

	
	public static final int UNLOCK_TRIGGER=4;
	/*
	 * Value not required, so TURN_ON
	 */
	public static final int LOW_BATTERY_TRIGGER=3;
	/*
	 * Value is the low battery value (percent/float)  
	 */
	public static final int INCOMING_CALL_TRIGGER=2;
	/*
	 * Value not required, so TURN_ON
	 */
	public static final int RINGER_MODE_TRIGGER=1;
	/*
	 * Value RINGER_SILENT, RINGER_NORMAL, RINGER_VIBRATE
	 */
	
	/**
	 * 
	 * CLASS MEMBERS
	 * 
	 */
	
	public String title;
	public Date date;
	public int sno1;
	public int sno2;
	public HashMap<Integer, String> triggers = new HashMap<Integer, String>();
	public HashMap<Integer, String> actions = new HashMap<Integer, String>();
	public boolean justAlerted=false;
//	public String actiontext;
//	public String triggertext;
}
	