package com.smartdo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.widget.Toast;
/**
 * Discarded. Was used for detecting Ringer Mode Changes. 
 * Discarded because it OS can end up giving multiple broadcasts. 
 * 
 * @author aref
 *
 */
public class RingerModeStateChangeReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		if(intent.getExtras().get(AudioManager.EXTRA_RINGER_MODE).equals(AudioManager.RINGER_MODE_NORMAL))
			Toast.makeText(context, "YO MAN.. WHY CHANGE? :P \n NORMAL", Toast.LENGTH_SHORT).show();
		else if(intent.getExtras().get(AudioManager.EXTRA_RINGER_MODE).equals(AudioManager.RINGER_MODE_SILENT))
			Toast.makeText(context, "YO MAN.. WHY CHANGE? :P \n SILENT", Toast.LENGTH_SHORT).show();
		else if(intent.getExtras().get(AudioManager.EXTRA_RINGER_MODE).equals(AudioManager.RINGER_MODE_VIBRATE))
			Toast.makeText(context, "YO MAN.. WHY CHANGE? :P \n VIBRATE", Toast.LENGTH_SHORT).show();

//		intent.getAction();
	}

}
