package com.smartdo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;
/**
 * Broadcast receiver for detecting an incoming call and can get the incoming caller's number 
 * 
 * @author aref
 *
 */
public class IncomingCallReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.i("IncomingCallReceiver ", "IncomingCallReceiver ");
		Toast.makeText(context, ""+ intent.getExtras().get(TelephonyManager.EXTRA_INCOMING_NUMBER), Toast.LENGTH_SHORT).show();
		AutomationHandler.doAutoIncomingCallBR(context);	 
	}

}
