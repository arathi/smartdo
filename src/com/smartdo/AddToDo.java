package com.smartdo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.smartdo.files.BookmarkFileProcessor;
import com.smartdo.files.TodoFileProcessor;
/**
 * The following class adds a new Todo
 * It creates a  new TodoItem object and sets its fields by taking
 * the values of those fields from the UI activity
 * @author Amrit
 * 
 * 
 */
public class AddToDo extends Activity {

	int mYear, mMonth, mDay;
	int mHour, mMinute;
	Button save;
	Button cancel;
	Button showMap;
	Button showBookmarks;
	TextView timeView;
	TextView dateView;
	EditText title;
	TodoItem temp = new TodoItem();
	TextView todoContact;
	TextView todoLocation; 
	TextView todoSelectedLocation;
	Date date1;
	EditText todoRadius;
	double lat;
	double longi;
	Context context = this;
	Random gen = new Random();
	boolean sms;
	List<String> list; //location spinner list
	boolean locationSelected=false;
	AlertDialog.Builder bookmarkBuilder;
	Calendar c;
	boolean timeChanged=false;
	boolean dateChanged=false;
	@Override
	/**
	 * @author Amrit
	 * @param savedInstanceState
	 * This method implements click handlers of various buttons
	 * 
	 */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_to_do);

		// getting resources from view
		bookmarkBuilder = new AlertDialog.Builder(this);
		timeView = (TextView) findViewById(R.id.todoTime);
		dateView = (TextView) findViewById(R.id.todoDate);
		title = (EditText) findViewById(R.id.todo_title_editText);
		todoContact = (TextView) findViewById(R.id.todoContact);
		todoLocation = (TextView) findViewById(R.id.todoLocation);
		todoRadius = (EditText) findViewById(R.id.editText1);
		save = (Button) findViewById(R.id.saveButtonAddtodo);
		cancel = (Button) findViewById(R.id.cancelButtonAddtodo);
		showMap=(Button)findViewById(R.id.button1);
		showBookmarks=(Button)findViewById(R.id.button2);
		title.setText((String)getIntent().getStringExtra("title"));
		list=new ArrayList<String>();
		todoSelectedLocation=(TextView)findViewById(R.id.todoSelectedLocation);
		temp.assigned_to = "me";

		
		// getting calendar for date and time picker
		c = Calendar.getInstance();
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);
		mHour = c.get(Calendar.HOUR_OF_DAY);
		mMinute = c.get(Calendar.MINUTE);
		date1 = new Date();
		
		showBookmarks.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				bookmarkBuilder.create();
				bookmarkBuilder.show();
			}
		});
		bookmarkBuilder.setTitle("Select a Bookmark")
		.setItems(BookmarkFileProcessor.getTitles(), new DialogInterface.OnClickListener(){

			@Override
			public void onClick(DialogInterface dialog, int which) {
				lat=BookmarkFileProcessor.bookmarkItems.get(which).latitude;
				longi=BookmarkFileProcessor.bookmarkItems.get(which).longitude;
				Log.e("DIALOG", ""+lat);
				Log.e("DIALOG", ""+longi);
				temp.latitude=lat;
				temp.longitude=longi;
				todoSelectedLocation.setText(BookmarkFileProcessor.bookmarkItems.get(which).title);
			}
		});
		
		save.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				temp.title = title.getText().toString();
				temp.author = "me";
				temp.due_date = date1;
				if(!todoRadius.getText().toString().isEmpty()){
					temp.radius = Float.parseFloat(todoRadius.getText().toString());
				}
				else{
					temp.radius=0;
				}
				temp.sno1 = gen.nextInt(1000);
				temp.sno2 = gen.nextInt(1000);
				
				Log.i("AddTodo","SNO1 "+temp.sno1+" SNO2 "+temp.sno2);
				
				if(temp.title.equals("") || temp.due_date.toString().equals("") || temp.radius==0 
						|| temp.latitude==0 || timeChanged==false || dateChanged==false){
					Toast.makeText(context, "Plese enter all Fields!", Toast.LENGTH_SHORT).show();
				}
				else{
					if (temp.assigned_to.equals("me")) {
						Toast.makeText(context, "todoContact is null!",
								Toast.LENGTH_SHORT).show();
						//temp.assigned_to="me";
						Log.d("contact-IF", "" + temp.assigned_to);
					} else {
						Toast.makeText(context, "SENDING TODO!", Toast.LENGTH_SHORT)
								.show();
						
						Log.d("contact-ELSE", "" + temp.assigned_to);
						SmsMgr.SMSSend(context, temp);
					}
					writeTodo(temp);
					setAlarm(temp);
					finish();
				}
			}
		});
		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				temp = null;
				finish();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_add_to_do, menu);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@SuppressWarnings("deprecation")
	/**
	 * @author Amrit
	 * @param view
	 */
	public void showDatePicker(View view) {
		showDialog(0);
	}

	@SuppressWarnings("deprecation")
	/**
	 * @author Amrit
	 * @param view
	 */
	public void showTimePicker(View view) {
		showDialog(999);
	}
    /**
     * @author Amrit
     * The date picker dialog implementation
     */
	private DatePickerDialog.OnDateSetListener mDateSetListner = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			dateChanged=true;
			Log.d("month: ", "" + monthOfYear);
			mDay = dayOfMonth;
			mYear = year;
			mMonth = monthOfYear;
			int newMonth = mMonth + 1;
			dateView.setText(mDay + "-" + newMonth + "-" + mYear);
		}
	};
	 /**
     * @author Amrit
     * The time picker dialog implementation
     */
	private TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {

		@SuppressWarnings("deprecation")
		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			timeChanged=true;
			mHour = hourOfDay;
			mMinute = minute;
			int h = mHour;
			date1.setHours(h);
			date1.setMinutes(minute);
			date1 = new Date(mYear - 1900, mMonth, mDay, mHour, mMinute);
			timeView.setText(" " + mHour + ":" + mMinute);
			Log.d("DT: ", date1.toString());
		}
	};

	/**
	 * @author Amrit
	 * @param id
	 */
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case 0:
			return new DatePickerDialog(this, mDateSetListner, mYear, mMonth,
					mDay);
		case 999:
			return new TimePickerDialog(this, mTimeSetListener, mHour, mMinute,
					false);
		}
		return null;
	}

	public void writeTodo(TodoItem todo) {
		TodoFileProcessor.writeTodo(this, todo);
	}

	@Override
	public void finish() {
		if (temp == null) {
			setResult(0);
			super.finish();
		} else {
			setResult(1002);
			super.finish();
		}
	}
	/**
	 * @author Amrit
	 * @param view
	 * Starts a new Intent for selecting Contact
	 */
	public void contactPicker(View view) {
		Intent intent = new Intent(Intent.ACTION_PICK,
				ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
		startActivityForResult(intent, 1001);
	}
	
	/**
	 * 
	 * @author Amrit
	 * @param reqcode rescode data
	 * This method is called when a contact is chosen by the user
	 * and the assigned_to field of the TodoItem is set here
	 * It also sets the latitude and longitude of the TodoItem
	 * when the user returns from the map and selects a location there
	 */
	
	protected void onActivityResult(int reqcode, int rescode, Intent data) {
		super.onActivityResult(reqcode, rescode, data);

		if (reqcode == 1001 && rescode == RESULT_OK) {
			switch (reqcode) {
			case 1001:
				Uri result = data.getData();
				int type = ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE;

				String[] whereArgs = new String[] {
						String.valueOf(result.getLastPathSegment()),
						String.valueOf(type) };

				Cursor cursor = getContentResolver().query(
						ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
						null,
						ContactsContract.CommonDataKinds.Phone._ID
								+ " = ? and "
								+ ContactsContract.CommonDataKinds.Phone.TYPE
								+ " = ?", whereArgs, null);
				int phoneNumberIndex = cursor
						.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER);
				if (cursor != null) {
					try {
						if (cursor.moveToFirst()) {
							String phoneNumber = cursor
									.getString(phoneNumberIndex);
							Log.d("contact", phoneNumber);
							// temp.setMobileNo(phoneNumber);
							// temp.setAssigned_to(phoneNumber);
							//sms=true;
							temp.assigned_to = phoneNumber;
							todoContact.setText(temp.assigned_to);
						}
						else {
							//sms=false;
							temp.assigned_to = "me";
							Log.w("contacts picker", "Warning: activity result not ok");
						}

					} finally {
						cursor.close();
					}
					
				}

				break;
			}
		} 
		if (rescode == 5001 && reqcode == 5000) {
			lat = data.getDoubleExtra("lat", 0);
			longi = data.getDoubleExtra("longi", 0);
			temp.latitude = lat;
			temp.longitude = longi;
			if(lat==0){
				Toast.makeText(context, "No location selected on map!", Toast.LENGTH_SHORT).show();
			}
			else{
				todoLocation.setText(lat + "-" + longi);
				todoSelectedLocation.setText("Selected on Map");
			}
		}

	}
	/**
	 * @author Aref
	 * @param todoItem
	 * This methods sets the alarm for time and sets proximity alerts for the todoItem
	 */
	public void setAlarm(TodoItem todoItem) {
		AlaManager.addAlarm(context, todoItem);
	}

	/**
	 * @author Amrit
	 * @param view
	 * Starts the MapsProcessor activity to select the location of the TodoItem
	 */
	public void getPoints(View view) {
		Intent intent = new Intent(this, MapProcessor.class);
		intent.putExtra("action", "todo");
		startActivityForResult(intent, 5000);
	}
	/**
	 * @author Amrit
	 * Starts the MapsProcessor activity to select the location of the TodoItem
	 */
	public void getPoints() {
		Intent intent = new Intent(this, MapProcessor.class);
		intent.putExtra("action", "todo");
		startActivityForResult(intent, 5000);
	}
}
