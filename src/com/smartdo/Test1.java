package com.smartdo;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TextView;

public class Test1 extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test1);
		TextView h=(TextView)findViewById(R.id.test);
		@SuppressWarnings("unchecked")
		ArrayList<TodoItem> todos=(ArrayList<TodoItem>) getIntent().getSerializableExtra("todos");
		h.setText(todos.get(0).getTitle());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_test1, menu);
		return true;
	}

}
