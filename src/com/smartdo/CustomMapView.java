package com.smartdo;

import org.mapsforge.android.maps.MapView;
import org.mapsforge.android.maps.mapgenerator.MapGenerator;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.GestureDetector.SimpleOnGestureListener;
/*
 * This class extends MapView to add variuos Gestures to the base map
 * @author Aref
 * */
public class CustomMapView extends MapView {

	Context context;
	private GestureDetector gestureDetector;

	
	public CustomMapView(Context context) {
		super(context);
		this.context=context;
		setGesture();

	}

	public CustomMapView(Context context, MapGenerator mapGenerator) {
		super(context, mapGenerator);
		this.context=context;
		setGesture();

	}
	

	public CustomMapView(Context context, AttributeSet attributeSet) {
		super(context, attributeSet);
		this.context=context;
		setGesture();

	}

	@SuppressWarnings("deprecation")
	public void setGesture(){
		gestureDetector = new GestureDetector(
                new SimpleOnGestureListener(){
                	 private static final int SWIPE_MIN_DISTANCE = 200;
                	    private static final int SWIPE_MAX_OFF_PATH = 80;
                	    private static final int SWIPE_THRESHOLD_VELOCITY = 800;

                	    @Override
                	    public boolean onFling(MotionEvent e1, MotionEvent e2,
                	                         float velocityX, float velocityY) {
                	      try {
                	        float diffAbs = Math.abs(e1.getY() - e2.getY());
                	        float diff = e1.getX() - e2.getX();

                	        if (diffAbs > SWIPE_MAX_OFF_PATH)
                	          return false;
                	        
                	        // Left swipe
                	        if (diff > SWIPE_MIN_DISTANCE
                	        && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                	        	
                	    		showTodo();

                	        	Log.i("YOOOOOOO","SWIPE DETECTED ON LIST"+velocityX);
                	        }
                	        // Right swipe
                	        else if (-diff > SWIPE_MIN_DISTANCE
                	        && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                	        	showAuto();
                	        	Log.i("YOOOOOOO","SWIPE DETECTED ON LIST"+velocityX);
                	        	}
                	      } catch (Exception e) {
                	        Log.e("MainActivity", "Error on gestures");
                	      }
                	      return false;
                	    }
                	  }
                );
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {		
//		Log.i("motion", "yup");
	    if (gestureDetector.onTouchEvent(event)) {
	      return true;
	    }
	    return super.onTouchEvent(event);
	}
	
	public void showTodo(){
		if(MapProcessor.caller.equals("todo"))
			((Activity)context).finish();
		else{
			Intent intent = new Intent(context, MainActivity.class);
			intent.putExtra("anim","right");
			intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
			context.startActivity(intent);
		}
	}
	
	public void showAuto(){
		if(MapProcessor.caller.equals("auto"))
			((Activity)context).finish();
		else{
			Intent intent = new Intent(context, AutomationHome.class);
			intent.putExtra("anim","left");
			intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
			context.startActivity(intent);
		}	
	}
	
}
