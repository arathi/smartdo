package com.smartdo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.widget.ListView;

public class AutoCustomListView extends ListView {

	Context context;

	public AutoCustomListView(Context context, AttributeSet attrs) {
		super(context,attrs);
		this.context=context;
		Log.i("TCListView", "Const1");
		setGesture();
	}
	
	public AutoCustomListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		Log.i("TCListView", "Const2");
		this.context=context;
		setGesture();
	}

	public AutoCustomListView(Context context) {
		super(context);
		Log.i("TCListView", "Const2");
		this.context=context;
		setGesture();
	}

	
	private GestureDetector gestureDetector;

	
	@SuppressWarnings("deprecation")
	public void setGesture(){
		gestureDetector = new GestureDetector(
                new SimpleOnGestureListener(){
                	 private static final int SWIPE_MIN_DISTANCE = 120;
                	    private static final int SWIPE_MAX_OFF_PATH = 200;
                	    private static final int SWIPE_THRESHOLD_VELOCITY = 200;

                	    @Override
                	    public boolean onFling(MotionEvent e1, MotionEvent e2,
                	                         float velocityX, float velocityY) {
                	      try {
                	        float diffAbs = Math.abs(e1.getY() - e2.getY());
                	        float diff = e1.getX() - e2.getX();

                	        if (diffAbs > SWIPE_MAX_OFF_PATH)
                	          return false;
                	        
                	        // Left swipe
                	        if (diff > SWIPE_MIN_DISTANCE
                	        && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                	        	showMap();
//                	        	Log.i("YOOOOOOO","SWIPE DETECTED ON LIST");
                	        }
                	        // Right swipe
                	        else if (-diff > SWIPE_MIN_DISTANCE
                	        && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                	        	showTodo();
//                	        	Log.i("YOOOOOOO","SWIPE DETECTED ON LIST");
                	        	}
                	      } catch (Exception e) {
                	        Log.e("MainActivity", "Error on gestures");
                	      }
                	      return false;
                	    }
                	  }
                );
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {		
//		Log.i("motion", "yup");
	    if (gestureDetector.onTouchEvent(event)) {
	      return true;
	    }
	    return super.onTouchEvent(event);
	}

	public void showMap(){
		Intent intent=new Intent(context, MapProcessor.class);
		intent.putExtra("action", "overlays");
		intent.putExtra("anim","right");
		intent.putExtra("caller","auto");
		context.startActivity(intent);
	}
	
	public void showTodo(){
		((Activity)context).finish();
	}
}
