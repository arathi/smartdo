package com.smartdo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Bundle;
import android.util.Log;
/**
 * Discarded. All this class's functionality is implemented in AutomationHandler. 
 * 
 * @author aref
 *
 */
public class AutoItemProcess extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.i("Broadcast Received", "AutoItemProcess");
		Bundle b = intent.getExtras();
		AutomationItem autoI= (AutomationItem) b.getSerializable("AutomationItem");
		Log.e("AutoItemProcess",autoI.title);
		
		switch (intent.getIntExtra("CurrentType", 0)){
		case 0:
			Log.e("AutoItemProcess", "0 in CurrentType");
			break;
			
		case AutomationItem.LOCATION_TRIGGER:
			Log.e("AutoItemProcess", "Location Trigger in CurrentType switch");
			AutomationHandler.checkOtherTriggers(context, autoI, AutomationItem.LOCATION_TRIGGER);
			break;
			
		case AutomationItem.ALARM_TRIGGER:
			Log.e("AutoItemProcess", "Alarm Trigger in CurrentType switch");
			AutomationHandler.checkOtherTriggers(context, autoI, AutomationItem.ALARM_TRIGGER);
			break;
			
		case AutomationItem.UNLOCK_TRIGGER:
			//TODO
			break;
			
		case AutomationItem.INCOMING_CALL_TRIGGER:
			//TODO
			break;
			
		case AutomationItem.RINGER_MODE_TRIGGER:
			//TODO
			break;
			
		case AutomationItem.LOW_BATTERY_TRIGGER:
			
			break;
		}
		
		
		
//		NotificationManager nm;
//		nm=(NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
//		Notification n = new Notification(R.drawable.ic_launcher, todo.getTitle(), System.currentTimeMillis());
//		n.defaults = Notification.DEFAULT_ALL;
//
//		Intent notificationIntent = new Intent(context, AlertAct.class);
//		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//		notificationIntent.putExtra("Todo", todo);
//		//notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//		//notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//		Log.i("ENTERING::::", "" +  b.getBoolean("entering"));
//		notificationIntent.putExtra("entering", b.getBoolean("entering"));
//		
//		PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent,  0);
//		n.setLatestEventInfo(context, todo.getTitle(), todo.getAuthor(), contentIntent);	
//		nm.notify(0,n);
	}

}
