package com.smartdo.files;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import android.content.Context;
import android.util.Log;

public class MyNumberFileProcessor {
	static File mobileNumberFile;
	public static String myNumber=new String();
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	public static String getMobileNumber(Context context){
		try{
			if(mobileNumberFile==null){
				mobileNumberFile=new File(context.getExternalFilesDir(null), "MobileNumber.txt");
			}
			FileInputStream fis=new FileInputStream(mobileNumberFile);
			ObjectInputStream ois=new ObjectInputStream(fis);
			myNumber=(String)ois.readObject();
		    
		    fis.close();
		}catch(IOException e){Log.d("getMobileNumber()", e.toString());} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		}
		return myNumber;
	}
	
	public static void updateMobileNumber(Context context, String number){
		try{
			if(mobileNumberFile==null){
				mobileNumberFile=new File(context.getExternalFilesDir(null), "MobileNumber.txt");
			}
				FileOutputStream fos= new FileOutputStream(mobileNumberFile);
				ObjectOutputStream oos=new ObjectOutputStream(fos);
				oos.writeObject(number);
				oos.close();
				fos.close();
			}catch(Exception e){Log.d("updateMobileNumber()", e.toString());}
		Log.d("MyNunmber", getMobileNumber(context));
	}
}
