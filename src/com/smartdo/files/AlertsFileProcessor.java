package com.smartdo.files;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

import android.util.Log;
public class AlertsFileProcessor {
	

	public static HashMap getTodoAlertStatus(String dir){
		HashMap todoAlertHash= new HashMap();
		try{
			File newTodoAlertFile=new File(dir, "TodoAlertStatus.txt");
			FileInputStream fis=new FileInputStream(newTodoAlertFile);
			ObjectInputStream ois=new ObjectInputStream(fis);
			todoAlertHash = (HashMap)ois.readObject();
			fis.close();
			ois.close();
		}catch(IOException e){Log.d("getTodoAlertStatus IO Exception", e.toString());}
		catch(ClassNotFoundException e1){Log.d("getTodoAlertStatus Exception", e1.toString());
		}
		return todoAlertHash;
	}

	public static void writeTodoAlertStatus(String dir,	HashMap todoAlertsStatus) {
		try{
			File newTodoAlertFile=new File(dir, "TodoAlertStatus.txt");
			FileOutputStream fos= new FileOutputStream(newTodoAlertFile);
			ObjectOutputStream oos=new ObjectOutputStream(fos);
			oos.writeObject(todoAlertsStatus);
			oos.close();
			fos.close();
		}catch(Exception e){ Log.e("Exception in writeTodoAlertStatus", e.toString());}
	}
	
	public static HashMap getAutoAlertStatus(String dir){
		HashMap todoAlertHash= new HashMap();
		try{
			File newAutoAlertFile=new File(dir, "AutoAlertStatus.txt");
			FileInputStream fis=new FileInputStream(newAutoAlertFile);
			ObjectInputStream ois=new ObjectInputStream(fis);
			todoAlertHash = (HashMap)ois.readObject();
			fis.close();
			ois.close();
		}catch(IOException e){Log.d("getAutoAlertStatus IO Exception", e.toString());}
		catch(ClassNotFoundException e1){Log.d("getAutoAlertStatus Exception", e1.toString());
		}
		return todoAlertHash;
	}

	public static void writeAutoAlertStatus(String dir,	HashMap autoAlertsStatus) {
		try{
			File newAutoAlertFile=new File(dir, "AutoAlertStatus.txt");
			FileOutputStream fos= new FileOutputStream(newAutoAlertFile);
			ObjectOutputStream oos=new ObjectOutputStream(fos);
			oos.writeObject(autoAlertsStatus);
			oos.close();
			fos.close();
		}catch(Exception e){ Log.e("Exception in writeAutoAlertStatus", e.toString());}
	}
}
