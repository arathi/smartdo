package com.smartdo.files;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import android.content.Context;
import android.util.Log;

import com.smartdo.AlaManager;
import com.smartdo.LocaManager;
import com.smartdo.TodoItem;

/*import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

import android.util.Log;

import com.smartdo.MainActivity;


public class TodoFileProcessor {
	static File todoFile;
	public static ArrayList<String> todoLines;
	public TodoFileProcessor(){
		todoFile=MainActivity.todoFile;
		todoLines=new ArrayList<String>();
		getTodos();
	}
	
	public static ArrayList<String> getTodos(){
		todoLines.clear();
		try {
			FileInputStream fis=new FileInputStream(todoFile);
			Scanner s=new Scanner(fis);
			while(s.hasNextLine()){
				todoLines.add(s.nextLine());
			}
		} catch (FileNotFoundException e1) {e1.printStackTrace();}
		for(int i=0;i<todoLines.size();i++){
			Log.d("Lines", todoLines.get(i));
		}
		return todoLines;
	}
	
	public static void writeTodo(String todoString){
		todoLines.add(todoString);
		try{
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(todoFile, true)));
		    out.println(todoString);
		    out.close();
		}catch (IOException e2) {e2.printStackTrace();}
	}
	
	public static void deleteTodo(String todoString){
		try{
			for(int i=0;i<todoLines.size();i++){
				if(todoLines.get(i).equals(todoString)){
					todoLines.remove(i);
				}
			}
			
			for(int i=0;i<todoLines.size();i++){
				PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(todoFile, false)));
			    out.println(todoLines.get(i)+"\n");
			    out.close();
			}
		}catch(IOException e3){e3.printStackTrace();}
	}
}*/


public class TodoFileProcessor{
	static File newTodoFile;
	public static ArrayList<TodoItem> todoItems=new ArrayList<TodoItem>();;
	public TodoFileProcessor(){
	}
	
	@SuppressWarnings("unchecked")
	public static ArrayList<TodoItem> getTodos(Context context){
		try{
			if(newTodoFile==null){
				newTodoFile=new File(context.getExternalFilesDir(null), "SmartTodo.txt");
			}
			FileInputStream fis=new FileInputStream(newTodoFile);
			ObjectInputStream ois=new ObjectInputStream(fis);
			todoItems= (ArrayList<TodoItem>)ois.readObject();
			fis.close();
			ois.close();
		}catch(IOException e){Log.d("getTodos()", e.toString());}
		catch(ClassNotFoundException e1){Log.d("getTodos()", e1.toString());
		}
		return todoItems;
	}
	
	@SuppressWarnings("unchecked")
	public static ArrayList<TodoItem> getTodos(String dirPath){
		try{
			if(newTodoFile==null){
				newTodoFile=new File(dirPath, "SmartTodo.txt");
			}
			FileInputStream fis=new FileInputStream(newTodoFile);
			ObjectInputStream ois=new ObjectInputStream(fis);
			todoItems= (ArrayList<TodoItem>)ois.readObject();
			fis.close();
			ois.close();
		}catch(IOException e){Log.d("getTodos()", e.toString());}
		catch(ClassNotFoundException e1){Log.d("getTodos()", e1.toString());
		}
		return todoItems;
	}
	public static void writeTodo(Context context, TodoItem todo){
		try{
			if(todoItems==null){
				getTodos(context);
			}
			todoItems.add(todo);Log.d("adding todo2", todoItems.get(todoItems.size()-1).getTitle());
			
			FileOutputStream fos= new FileOutputStream(newTodoFile);
			ObjectOutputStream oos=new ObjectOutputStream(fos);
			oos.writeObject(todoItems);
			oos.close();
			fos.close();
			Log.d("adding todo", "added");
		}catch(Exception e){Log.d("writeTodo()", e.toString());}
	}
	
	public static void deleteTodo(Context context, TodoItem todo){
		try{
			if(todoItems==null){
				getTodos(context);
			}
			LocaManager.delProx(context, todo);
			AlaManager.remAlarm(context, todo);
			todoItems.remove(todo);
			FileOutputStream fos= new FileOutputStream(newTodoFile);
			ObjectOutputStream oos=new ObjectOutputStream(fos);
			oos.writeObject(todoItems);
			oos.close();
			fos.close();
		}catch(Exception e){Log.d("deteleTodo()", e.toString());}
	}
	
}