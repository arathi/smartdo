package com.smartdo.files;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import android.content.Context;
import android.util.Log;

import com.smartdo.BookmarkItem;

public class BookmarkFileProcessor {
	static File bookmarksFile;
	public static ArrayList<BookmarkItem> bookmarkItems=new ArrayList<BookmarkItem>();
	
	@SuppressWarnings("unchecked")
	public static ArrayList<BookmarkItem> getBookmarks(Context context){
		try{
			if(bookmarksFile==null){
				bookmarksFile=new File(context.getExternalFilesDir(null), "Smartbms.txt");
			}
			FileInputStream fis=new FileInputStream(bookmarksFile);
			ObjectInputStream ois=new ObjectInputStream(fis);
			bookmarkItems= (ArrayList<BookmarkItem>)ois.readObject();
			fis.close();
			ois.close();
		}catch(IOException e){Log.d("getBookmarks()", e.toString());}
		catch(ClassNotFoundException e1){Log.d("getBookmarks()", e1.toString());
		}
		return bookmarkItems;
	}
	
	public static void writeBookmark(Context context, BookmarkItem bookmark){
		try{
			if(bookmarkItems==null || bookmarkItems.isEmpty()){
				getBookmarks(context);
			}
			bookmarkItems.add(bookmark);
			FileOutputStream fos= new FileOutputStream(bookmarksFile);
			ObjectOutputStream oos=new ObjectOutputStream(fos);
			oos.writeObject(bookmarkItems);
			oos.close();
			fos.close();
		}catch(Exception e){Log.d("writeBookmark()", e.toString());}
	}
	
	public static void deleteBookmark(Context context, BookmarkItem bookmark){
		try{
			if(bookmarkItems==null){
				getBookmarks(context);
			}
			bookmarkItems.remove(bookmark);
			FileOutputStream fos= new FileOutputStream(bookmarksFile);
			ObjectOutputStream oos=new ObjectOutputStream(fos);
			oos.writeObject(bookmarkItems);
			oos.close();
			fos.close();
		}catch(Exception e){Log.d("deteleBookmark()", e.toString());}
	}
	
	public static String[] getTitles(){
		String[] titles=new String[100];
		if(bookmarkItems.isEmpty()){
			return null;
		}
		else{
			for(int i=0;i<bookmarkItems.size();i++){
				titles[i]=bookmarkItems.get(i).title;
			}
		}
		return titles;
	}
}
