package com.smartdo.files;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import android.content.Context;
import android.util.Log;

import com.smartdo.AutomationItem;

public class AutoFileProcessor {
	static File newAutoFile;
	static File autoFile;
	public static ArrayList<AutomationItem> automationItems=new ArrayList<AutomationItem>();;
	public AutoFileProcessor(){
	}
	
	@SuppressWarnings("unchecked")
	public static ArrayList<AutomationItem> getAutos(Context context){
		try{
			if(newAutoFile==null){
				//create new file
				newAutoFile=new File(context.getExternalFilesDir(null), "SmartAuto.txt");
			}
			//otherwise read auto's and output it 
			FileInputStream fis=new FileInputStream(newAutoFile);
			ObjectInputStream ois=new ObjectInputStream(fis);
			automationItems= (ArrayList<AutomationItem>)ois.readObject();
			fis.close();
			ois.close();
		}catch(IOException e){Log.d("getAutos()", e.toString());}
		catch(ClassNotFoundException e1){Log.d("getAutos()", e1.toString());
		}
		return automationItems;
	}
	
	@SuppressWarnings("unchecked")
	public static ArrayList<AutomationItem> getAutos(String dirPath){
		try{
			if(newAutoFile==null){
				//create new file
				newAutoFile=new File(dirPath, "SmartAuto.txt");
				Log.i("AutoFileProcessor", "newAutoFile init "+newAutoFile);
			}
			
			
			//otherwise read auto's and output it 
			FileInputStream fis=new FileInputStream(newAutoFile);
			ObjectInputStream ois=new ObjectInputStream(fis);
			automationItems= (ArrayList<AutomationItem>)ois.readObject();
			fis.close();
			ois.close();
		}catch(IOException e){Log.d("getAutos()", e.toString());}
		catch(ClassNotFoundException e1){Log.d("getAutos()", e1.toString());
		}
		return automationItems;
	}
	
	public static void writeAuto(Context context, AutomationItem auto){
		try{
			if(automationItems==null){
				getAutos(context);
			}
			//read from object stream and write he object in file
			automationItems.add(auto);
			FileOutputStream fos= new FileOutputStream(newAutoFile);
			ObjectOutputStream oos=new ObjectOutputStream(fos);
			oos.writeObject(automationItems);
			oos.close();
			fos.close();
		}catch(Exception e){Log.d("writeAuto()", e.toString());}
	}
	
	public static void deleteAuto(Context context, AutomationItem auto){
		try{
			if(automationItems==null){
				getAutos(context);
			}
			automationItems.remove(auto);
			FileOutputStream fos= new FileOutputStream(newAutoFile);
			ObjectOutputStream oos=new ObjectOutputStream(fos);
			oos.writeObject(automationItems);
			oos.close();
			fos.close();
		}catch(Exception e){Log.d("deteleAuto()", e.toString());}
	}

}
