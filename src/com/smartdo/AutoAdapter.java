package com.smartdo;


import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.smartdo.files.AutoFileProcessor;
/*
 * The following class extends the BaseAdapter class
 * and is used to bind the AutomationHome activity's ListView
 * @author Ashish
 * 
 */
public class AutoAdapter  extends BaseAdapter{
	static ArrayList<AutomationItem> automationItems;
	Context context;
	public AutoAdapter(Context context){
		this.context=context;
		automationItems=AutoFileProcessor.automationItems;
	}
	
	@Override
	public View getView(final int index, View view, final ViewGroup parent){
		
		if(view==null){
			LayoutInflater inflater=LayoutInflater.from(parent.getContext());
			view=inflater.inflate(R.layout.single_automation_item, parent, false);
		}
		
		
		TextView textView=(TextView) view.findViewById(R.id.auto_title1);
		AutomationItem temp=(AutomationItem) getItem(index);
		textView.setText(temp.title);
		
		final CheckBox checkBox=(CheckBox) view.findViewById(R.id.auto_complete);
		checkBox.setChecked(false);
checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				if(arg1){
					final AutomationItem item=(AutomationItem)getItem(index);
					
					AlertDialog.Builder alert=new AlertDialog.Builder(context);
					alert.setTitle("Delete?");
					alert
						.setMessage("Delete task?")
						.setCancelable(true)
						.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int arg1) {
								AutoFileProcessor.deleteAuto(context, item);
								update();
								dialog.dismiss();
							}
						})
					.setNegativeButton("No", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
							checkBox.setChecked(false);
						}
					});
					AlertDialog d= alert.create();
					d.show();
					
				}
				
			}
		});
		return view;
	}

	@Override
	public int getCount() {
		return AutoFileProcessor.automationItems.size();
	}

	@Override
	public Object getItem(int i) {
		return AutoFileProcessor.automationItems.get(i);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}
	
	public void update(){
		notifyDataSetChanged();
	}
}