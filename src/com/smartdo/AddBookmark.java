package com.smartdo;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.smartdo.files.BookmarkFileProcessor;
/**
 * The following class adds a new bookmark to the point
 * on map selected by the user
 * @author Amrit
 * 
 *
 */
public class AddBookmark extends Activity {
	Button save;
	Button cancel;
	EditText title;
	TextView coordinates;
	final Context context=this;
	BookmarkItem item;
	/**
	 * @author Amrit
	 * @param savedInstanceState
	 * @return void
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.activity_add_bookmark);
		save=(Button)findViewById(R.id.saveBookmarkButton);
		cancel=(Button)findViewById(R.id.cancelBookmarkButton);
		title=(EditText)findViewById(R.id.bookmarkTitle);
		coordinates=(TextView)findViewById(R.id.coordinates);
		final double lat=getIntent().getDoubleExtra("lat", 0);
		final double longi=getIntent().getDoubleExtra("long", 0);
		coordinates.setText(lat+"-"+longi);
		save.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				String bTitle=title.getText().toString();
				item=new BookmarkItem();
				item.title=bTitle;
				item.latitude=lat;
				item.longitude=longi;
				if(item.title.equals("")){
					Toast.makeText(context, "Title is missing!", Toast.LENGTH_SHORT).show();
				}
				else{
					BookmarkFileProcessor.writeBookmark(context, item);
					finish();
				}
			}
		});
		cancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				item=null;
				finish();
				
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_add_bookmark, menu);
		return true;
	}
	
	@Override
	public void finish(){
		if(item==null){
		}
		else{
			setResult(RESULT_OK);
			Toast.makeText(context, "Bookmark Saved!", Toast.LENGTH_SHORT).show();
		}
		super.finish();
	}

}
