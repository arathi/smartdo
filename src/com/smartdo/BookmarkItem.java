package com.smartdo;

import java.io.Serializable;
/*
 * This class hold all the parameters of a Bookmark
 * @author Amrit
 */
@SuppressWarnings("serial")
public class BookmarkItem implements Serializable{
	public String title;
	public double latitude;
	public double longitude;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
}
