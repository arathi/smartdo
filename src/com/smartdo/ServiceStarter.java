package com.smartdo;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
/**
 * Starts GPS Poll Services. 
 * 
 * @author aref
 *
 */
public class ServiceStarter extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
    	Log.i("In ServiceStarter", "GPSCheckService will be started");
        Intent i = new Intent(context.getApplicationContext(), GPSCheckService.class);
        i.putExtra("FILEDIR", ""+context.getExternalFilesDir(null));
        PendingIntent pI = PendingIntent.getService(context, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
        //context.startService(i);
        AlaManager.doEveryXMin(context, pI, 1);
        Log.i("In ServiceStarter", "AlarmEntry Done");
    }
}