package com.smartdo;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;
/**
 * SMS Manager - used for sending a TodoItem/its Confirmations as SMS. Also, Normal SMS Text (in case of automation)
 * 
 * @author aref
 *
 */
public class SmsMgr {
	public static void SMSSend(Context context, TodoItem t) {
		String smsBody = "$_SMARTDO_1_";
		smsBody = smsBody + t.getSno1() + "_" + t.getSno2() + "_"
				+ t.getTitle() + "_" + t.getDue_date().getTime() + "_"
				+ t.getLatitude() + "_" + t.getLongitude() + "_"
				+ t.getRadius() + "_" + t.getPriority();
		Toast.makeText(context, smsBody, Toast.LENGTH_SHORT).show();
		Log.i("SIZE OF TEXT", "" + smsBody.length());
		PendingIntent pi = PendingIntent.getBroadcast(context, 0, new Intent(
				"com.smsstatbr"), 0);
		SmsManager sms = SmsManager.getDefault();
		sms.sendTextMessage(t.assigned_to, null, smsBody, pi, null);
	}
	
	public static void SMSSend(Context context, TodoItem t, Boolean confirmation){
		String smsBody = "$_SMARTDO_2_";
		smsBody = smsBody + t.getSno1() + "_" + t.getSno2() + "_"
				+ confirmation;
		Toast.makeText(context, smsBody, Toast.LENGTH_SHORT).show();
		Log.i("SIZE OF TEXT", "" + smsBody.length());
		PendingIntent pi = PendingIntent.getBroadcast(context, 0, new Intent(
				"com.smsstatbr"), 0);
		SmsManager sms = SmsManager.getDefault();
		sms.sendTextMessage(t.author, null, smsBody, pi, null);
	}

	public static void SMSSend(Context context, String to, String smsBody) {
		PendingIntent pi = PendingIntent.getBroadcast(context, 0, new Intent(
				"com.smsstatbr"), 0);
		SmsManager sms = SmsManager.getDefault();
		sms.sendTextMessage(to, null, smsBody, pi, null);
	}
	
	public static void SMSSendTaskDone(Context context, TodoItem t){
		String smsBody = "$_SMARTDO_3_";
		smsBody = smsBody + t.getSno1() + "_" + t.getSno2() + "_";
				
		Toast.makeText(context, smsBody, Toast.LENGTH_SHORT).show();
		Log.i("Confirmation Sent. Message Size:", "" + smsBody.length());
		PendingIntent pi = PendingIntent.getBroadcast(context, 0, new Intent(
				"com.smsstatbr"), 0);
		SmsManager sms = SmsManager.getDefault();
		sms.sendTextMessage(t.author, null, smsBody, pi, null);			//TODO
		
	}
}
