package com.smartdo;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsManager;
import android.widget.Toast;
/**
 * Broadcast receiver for detecting the success/failure of SmartDo sending an SMS 
 * 
 * @author aref
 *
 */
public class SMSStatBR extends BroadcastReceiver {

	
	  public void onReceive(Context arg0, Intent arg1) 
      {
          int resultCode = getResultCode();
          switch (resultCode) 
          {
           case Activity.RESULT_OK:                      Toast.makeText(arg0, "SMS sent",Toast.LENGTH_LONG).show();
                                                         break;
          case SmsManager.RESULT_ERROR_GENERIC_FAILURE:  Toast.makeText(arg0, "Generic failure",Toast.LENGTH_LONG).show();
                                                         break;
          case SmsManager.RESULT_ERROR_NO_SERVICE:       Toast.makeText(arg0, "No service",Toast.LENGTH_LONG).show();
                                                         break;
          case SmsManager.RESULT_ERROR_NULL_PDU:         Toast.makeText(arg0, "Null PDU",Toast.LENGTH_LONG).show();
                                                         break;
          case SmsManager.RESULT_ERROR_RADIO_OFF:        Toast.makeText(arg0, "Radio off",Toast.LENGTH_LONG).show();
                                                         break;
          }
      }
	
}
