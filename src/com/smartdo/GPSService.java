package com.smartdo;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

/**
 * Service to get GPS data from receiver and continuously tries on basis of some parameters like good data set counts, total data set count, a timeout, etc. 
 * 
 * @author aref
 *
 */
public class GPSService extends Service {
	
	private static final String TAG = "AREF";
	LocationListener locationListener = new LocationListener(LocationManager.GPS_PROVIDER); 
	Location lastLocation;
	LocationManager locationManager = null;
    Intent callingIntent;
    int goodDataSetCount=0;
    int dataSetNo=0;
	Timer timer1;
    int shotFlag=0;
	
	//private static final int LOCATION_INTERVAL = 3000;
	//private static final float LOCATION_DISTANCE = 1f;
	 
	private class LocationListener implements android.location.LocationListener{
		public LocationListener(String provider){
			lastLocation = new Location(provider);
			Log.e(TAG, "LocationListener " + provider);
		}
		
		@Override
		public void onLocationChanged(Location location) {
			timer1.cancel();
			Log.e(TAG, "onLocationChanged "+location.toString());            
	        lastLocation.set(location);
			sendLocationMessage();
			shotFlag=1;
		}

		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub
			timer1.cancel();
	        Log.e(TAG, "onProviderDisabled: " + provider);            
	        lastLocation=null;
			sendLocationMessage();
		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub
	        Log.e(TAG, "onProviderEnabled: " + provider);
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
	        Log.e(TAG, "onStatusChanged: " + provider);
		}
			
	}
	
	public void sendLocationMessage() {
		if(callingIntent!=null){
			Bundle extras = callingIntent.getExtras();
			if(extras!=null){
				Messenger messenger = (Messenger) extras.get("MESSENGER");
				Message msg = Message.obtain();
				//msg.arg1=1;
				msg.obj=lastLocation;
				try{
					messenger.send(msg);
				}
				catch(Exception e){
					Log.e(getClass().getName(), "Exception sending message", e);
				}
			}

			//Log.i("TEMP", extras.getFloat("reqAccuracy")+ "" );

			if(lastLocation==null){
				/**
				 * NULL valued lastLocation indicates either a GPS Provider acquiring
				 * failure or a Provider disabled problem
				 */
				stopSelf();
			}
			else{
				/*
				 * Check done here so that even less accurate data is transmitted
				 * but only data satisfying the accuracy criteria will up the count
				 * basically, it'll display data with low accuracy but will not stop
				 * GPS acquiring until it gets "reqDatasets" no of data sets with 
				 * "reqAccuracy" accuracy
				 */
				if(lastLocation.getAccuracy()<extras.getFloat("reqAccuracy")){
					goodDataSetCount++;
				}
				
				dataSetNo++;
					
				if(goodDataSetCount==extras.getInt("reqDatasets") || dataSetNo==extras.getInt("maxTotalDataset")){																///STOP AFTER TRANSFERING 10 DATASETS //UPDATE MAY BE REQUIRED
					stopSelf();
				}
			}
			
		}
		else{
			Log.e("ERROR","callingIntent is null!");
		}
	}
	
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	public int onStartCommand(Intent intent, int flags, int startId ){
        Log.e(TAG, "Service Started..");
        super.onStartCommand(intent, flags, startId);
        callingIntent=new Intent(intent);
        try {
	    	
	    	/*locationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, locationListener, null);*/
	    	 
	    	int updateInterval=callingIntent.getExtras().getInt("updateInterval", 3000);
        	float updateDistance=callingIntent.getExtras().getFloat("updateDistance", 1f);
	    	
        	locationManager.requestLocationUpdates(
	                LocationManager.GPS_PROVIDER, updateInterval, updateDistance,
	                locationListener);    //CAN ALSO USE LOW TIME AND THEN UNLIST LISTENER
	    	
	    	timer1=new Timer();
	    	int timeout=callingIntent.getExtras().getInt("GPStimeout", 30000);
	        timer1.schedule(new GetLastLocation(), timeout);
	    	
	    } catch (java.lang.SecurityException ex) {
	        Log.i(TAG, "fail to request location update, ignore", ex);
	        lastLocation=null; 
	        sendLocationMessage();						 				//Transfer message with location null to indicate failure
	    } catch (IllegalArgumentException ex) {
	        Log.d(TAG, "gps provider does not exist " + ex.getMessage());
	        lastLocation=null; 
	        sendLocationMessage();										//Transfer message with location null to indicate failure
	    }
        return START_NOT_STICKY;			///If you kill it don't bother creating it again dude
	}
	
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		Log.e(TAG, "in onCreate");
		initLocManager();
	    
	    super.onCreate();
	}

	@Override
	public void onDestroy() {
		Log.e(TAG, "onDestory");
		super.onDestroy();
		if(locationManager!=null){
			try{
				locationManager.removeUpdates(locationListener);
			}
			catch(Exception e){
                Log.i(TAG, "Remove LocaListener failed", e);
			}
		}
	}
	
	private void initLocManager() {
		if (locationManager==null){
	        locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		}
	}

 

	class GetLastLocation extends TimerTask {
		@Override
		public void run() {
			Log.i("Timer Run", "Timer had run!");
			locationManager.removeUpdates(locationListener); 
			Location temp =locationManager.getLastKnownLocation(locationManager.GPS_PROVIDER);
			if(temp!=null){
				lastLocation.set(temp);
				Bundle extras = new Bundle();
				extras.putBoolean("Last Known", true);
				lastLocation.setExtras(extras);
			}
			else{
				lastLocation=null;
			}
			sendLocationMessage();
			stopSelf();
		}
	}
	
	
}
