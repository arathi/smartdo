//package com.smartdo;
//
//import android.app.Activity;
//import android.os.Bundle;
//import android.support.v4.app.NavUtils;
//import android.view.Menu;
//import android.view.MenuItem;
//
//public class Automation_details extends Activity {
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_automation_details);
////		TextView title=(TextView)findViewById(R.id.automation_title_string);
////		title.setText(getIntent().getStringExtra("item"));
////		// Show the Up button in the action bar.
//		//getActionBar().setDisplayHomeAsUpEnabled(true);
//	}
//
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.activity_automation_details, menu);
//		return true;
//	}
//
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		switch (item.getItemId()) {
//		case android.R.id.home:
//			// This ID represents the Home or Up button. In the case of this
//			// activity, the Up button is shown. Use NavUtils to allow users
//			// to navigate up one level in the application structure. For
//			// more details, see the Navigation pattern on Android Design:
//			//
//			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
//			//
//			NavUtils.navigateUpFromSameTask(this);
//			return true;
//		}
//		return super.onOptionsItemSelected(item);
//	}
//
//}


package com.smartdo;

import java.io.File;

import org.mapsforge.android.maps.MapActivity;
import org.mapsforge.android.maps.MapController;
import org.mapsforge.android.maps.MapView;
import org.mapsforge.android.maps.overlay.OverlayItem;
import org.mapsforge.core.GeoPoint;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
/**
 * @author Ashish
 * This class shows the details of an Automation task
 *
 */
@SuppressLint("NewApi")
public class AutomationDetails extends MapActivity {

	TextView trigger;
	TextView action;
	TextView title;
	String actiontext;
	String triggertext;
	AutomationItem ai;
	MapView map;
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_automation_details);
		
		title=(TextView) findViewById(R.id.title_string);
		trigger=(TextView) findViewById(R.id.textViewTrigger);
		action=(TextView) findViewById(R.id.textViewAction);
		
		ai=(AutomationItem) getIntent().getSerializableExtra("auto");
		
		Log.i("AddingAutomation", "TRIGGERS");
		Log.i("Ringer",
				"" + ai.triggers.get(AutomationItem.RINGER_MODE_TRIGGER));
		Log.i("ICall",
				"" + ai.triggers.get(AutomationItem.INCOMING_CALL_TRIGGER));
		Log.i("LBattery",
				"" + ai.triggers.get(AutomationItem.LOW_BATTERY_TRIGGER));
		Log.i("ULock", "" + ai.triggers.get(AutomationItem.UNLOCK_TRIGGER));
		Log.i("Alarm", "" + ai.triggers.get(AutomationItem.ALARM_TRIGGER));
		Log.i("GPS", "" + ai.triggers.get(AutomationItem.LOCATION_TRIGGER));

		Log.i("AddingAutomation->", "ACTIONS:");
		Log.i("Ringer", "" + ai.actions.get(AutomationItem.RINGER_MODE_ACTION));
		Log.i("Bluetooth", "" + ai.actions.get(AutomationItem.BLUETOOTH_ACTION));
		Log.i("Data", "" + ai.actions.get(AutomationItem.DATA_ACTION));
		Log.i("WIFI", "" + ai.actions.get(AutomationItem.WIFI_ACTION));
		Log.i("Brightness",
				"" + ai.actions.get(AutomationItem.SCREEN_BRIGHTNESS_ACTION));
		Log.i("SMSAction", " "+ ai.actions.get(AutomationItem.SMS_ACTION));
		
		if(ai.triggers.get(AutomationItem.RINGER_MODE_TRIGGER)!=null){
				
			switch (Integer.parseInt(ai.triggers.get(AutomationItem.RINGER_MODE_TRIGGER))) {
			case AutomationItem.RINGER_NORMAL:
				triggertext="Ringer mode=normal";
				break;

			case AutomationItem.RINGER_SILENT:
				triggertext="Ringer mode=silent";
				break;

			case AutomationItem.RINGER_VIBRATE:
				triggertext="Ringer mode=vibrate";
				break;
			}
			
		}else{
			triggertext="Ringer mode=normal";
		}
		
		if(ai.triggers.get(AutomationItem.INCOMING_CALL_TRIGGER)!=null){
			triggertext=triggertext+"\n"+"Incoming number="+ai.triggers.get(AutomationItem.INCOMING_CALL_TRIGGER);
		}
		
		if(ai.triggers.get(AutomationItem.LOW_BATTERY_TRIGGER)!=null){
			triggertext=triggertext+"\n"+"low battery="+ai.triggers.get(AutomationItem.LOW_BATTERY_TRIGGER)+"%";
		}
		
		if(ai.triggers.get(AutomationItem.UNLOCK_TRIGGER)!=null){
			triggertext=triggertext+"\n"+"phone="+ai.triggers.get(AutomationItem.UNLOCK_TRIGGER);
		}
		if(ai.triggers.get(AutomationItem.ALARM_TRIGGER)!=null){
			triggertext=triggertext+"\n"+"alarm="+ai.triggers.get(AutomationItem.ALARM_TRIGGER)+" "+"interval="+ai.triggers.get(AutomationItem.ALARM_TRIGGER_MARGIN);
		}
		if(ai.triggers.get(AutomationItem.LOCATION_TRIGGER)!=null){
			triggertext=triggertext+"\n"+"location="+ai.triggers.get(AutomationItem.LOCATION_TRIGGER);
		}
		
		title.setText(ai.title);
		
		trigger.setText(triggertext);
		
		if(ai.actions.get(AutomationItem.RINGER_MODE_ACTION)!=null){
			switch (Integer.parseInt(ai.actions.get(AutomationItem.RINGER_MODE_ACTION))) {

			case 0:
				actiontext="Ringer mode=silent";
				break;

			case 1:
				actiontext="Ringer mode=vibrate";
				break;
			
			case 2:
				actiontext="Ringer mode=normal";
				break;

			}

		}else{
			actiontext="Ringer mode=normal";
		}
		if(ai.actions.get(AutomationItem.BLUETOOTH_ACTION)!=null){
			actiontext=actiontext+"\n Bluetooth=ON";
		}
		if(ai.actions.get(AutomationItem.WIFI_ACTION)!=null){
			actiontext=actiontext+"\n Wifi=ON";
		}
		if(ai.actions.get(AutomationItem.DATA_ACTION)!=null){
			actiontext=actiontext+"\n Data=ON";
		}
		if(ai.actions.get(AutomationItem.SCREEN_BRIGHTNESS_ACTION)!=null){
			actiontext=actiontext+"\n Brightness=high";
		}
		if(ai.actions.get(AutomationItem.SMS_ACTION)!=null){
			actiontext=actiontext+"\n SMS="+ai.actions.get(AutomationItem.SMS_ACTION);
		}
		
		action.setText(actiontext);
		if(ai.triggers.get(AutomationItem.LOCATION_TRIGGER)!=null){
			showMapAutomation();
		}
		// Show the Up button in the action bar.
	//	getActionBar().setDisplayHomeAsUpEnabled(true);
	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_automation_details, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void showMapAutomation(){
		String location=ai.triggers.get(AutomationItem.LOCATION_TRIGGER);
		String[] coordinates=location.split(",");
		Double latitude=Double.parseDouble(coordinates[0]);
		Double longitude=Double.parseDouble(coordinates[1]);
		
		map=(MapView)findViewById(R.id.mapView_automation);
		int bottom=action.getBottom();
		Log.d("bottom", Integer.toString(bottom));
		map.setTop(bottom);
		map.setMapFile(new File("/mnt/sdcard/Indore.map"));
		map.setClickable(true);
		map.setBuiltInZoomControls(true);
		map.isFocusable();
		MapController mc=map.getController();
		Drawable defaultMarker = getResources()
				.getDrawable(R.drawable.mapmaker);
		ArrayItemizedOverlay arr = new ArrayItemizedOverlay(defaultMarker, this);
		GeoPoint geoPoint = new GeoPoint(latitude, longitude);
		map.setCenter(geoPoint);
		mc.setZoom(17);
		arr.addOverlay(new OverlayItem(geoPoint, "", ""));
		map.getOverlays().add(arr);
	}
}