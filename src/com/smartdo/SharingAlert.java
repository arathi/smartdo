package com.smartdo;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.smartdo.files.TodoFileProcessor;
/**
 * Alert to be displayed when shared todolist item is clicked in notification bar
 * 
 * @author aref
 *
 */
public class SharingAlert extends Activity {
	TodoItem temp;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sharealert);
		
		temp = (TodoItem) getIntent().getExtras().getSerializable("todo");
		TextView t = (TextView) findViewById(R.id.sharealertText);
		t.setText("You've got a todo request from "+temp.author+" to "+temp.title+" by "+temp.due_date);
		
		Button b = (Button) findViewById(R.id.sharealertaccept);
		b.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				accept();
				
			}
		});
		
		Button b2 = (Button) findViewById(R.id.sharealertdeny);
		b2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				deny();
				
			}
		});
	}

	void accept(){
		Log.i("SMSBR","Yes hit");
		TodoFileProcessor.writeTodo(this, temp);
		// ADD temp TO THE LIST HERE!!!
		TodoFileProcessor.todoItems.add(temp);
		//notify data set changed
		SmsMgr.SMSSend(this, temp, true);
		finish();
	}
	
	void deny(){
		SmsMgr.SMSSend(this, temp, false);
		finish();
	}
}
