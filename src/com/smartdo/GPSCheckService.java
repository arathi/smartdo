package com.smartdo;
import java.util.ArrayList;
import java.util.HashMap;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Handler;
import android.os.IBinder;
import android.os.Messenger;
import android.util.Log;

import com.smartdo.files.AlertsFileProcessor;
import com.smartdo.files.AutoFileProcessor;
import com.smartdo.files.TodoFileProcessor;

/**
 * This service runs every x Minutes and calls GPSService to get the geo-coordinates of the device.
 * 
 * @author aref
 *
 */
public class GPSCheckService extends Service {
	Intent serviceIntent;
	Intent callingIntent;
	int tryNo=0;
	int moreAccurateDataSet=0;
	ArrayList<AutomationItem> aIList ;
	ArrayList<TodoItem> todoList;
	
	//TODO - Memory leaks? -- https://groups.google.com/forum/?fromgroups=#!msg/android-developers/1aPZXZG6kWk/lIYDavGYn5UJ
	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			tryNo++;
			Location l = (Location) msg.obj;
			if (l != null) {
				Log.i("GPSCheckService",l.getLatitude() + "," + l.getLongitude() + ","
								+ l.getAccuracy());
				checkAutomations(l);
				checkTodo(l);
			} else {
				Log.i("GPSCheckService","Location object returned from service is null.");
						
			}
			if(tryNo==2){
				stopSelf();
			}
		};
	};

	
	private void checkAutomations(Location l){				//passing my location
		if(aIList==null){
			String fileDir=callingIntent.getExtras().getString("FILEDIR");
			if(fileDir==null)	Log.e("GPSCheckService -> checkAutomations", "FILEDIR was not found in intent! "+fileDir);
			aIList = AutoFileProcessor.getAutos(fileDir);	
			Log.i("GPSCheckService -> checkAutomations", "List populated");
		}
		Log.i("GPSCheckService -> checkAutomations", "Automation List Size "+aIList.size());
		for(AutomationItem aI : aIList){
			Log.i("GPSCheckService -> checkAutomations", "Checking automation item"+aI.title);
			if(aI.triggers.containsKey(AutomationItem.LOCATION_TRIGGER)){
				Log.i("GPSCheckService -> checkAutomations", "Satisfied. Checking other alert stats");
				automationAlertHandler(aI, isSatisfied(aI, l));
			}
		}
	}
	

	public void automationAlertHandler(AutomationItem aI, boolean update){
		HashMap autoAlertStatus ;
		boolean status;
		autoAlertStatus =	AlertsFileProcessor.getAutoAlertStatus(""+getApplicationContext().getExternalFilesDir(null));
		Log.i("GPSCheckService:automationAlertHandler", "HashMap size: "+autoAlertStatus.size());
		Object previousStatus = autoAlertStatus.put(aI.sno1, update);
		
		if(previousStatus==null){
			Log.i("GPSCheckService:automationAlertHandler", "No previous record in AlertStatusFile");
		}
		
		if((previousStatus!=null) && ((Boolean) previousStatus == update)){
			Log.i("GPSCheckService:automationAlertHandler", "No alerts or updates!");
			return;		//No updates & No alert!
		}
		else{
			if(update){
				Log.i("GPSCheckService:automationAlertHandler", "All OK. Checking other triggers!");
				AutomationHandler.checkOtherTriggers(getApplicationContext(),aI, AutomationItem.LOCATION_TRIGGER);
			}
			else{
				Log.i("GPSCheckService:automationAlertHandler", "Only updates!");
			}
			AlertsFileProcessor.writeAutoAlertStatus(""+getApplicationContext().getExternalFilesDir(null), autoAlertStatus);
		}
	}
	
	
	boolean isSatisfied (AutomationItem aI, Location l){
		String locStr=aI.triggers.get(AutomationItem.LOCATION_TRIGGER);
		String [] temp = locStr.split(",");
		Location aILoc = new Location("aILoc");
		int isInverted=0;
		aILoc.setLatitude(Double.parseDouble(temp[0]));
		aILoc.setLongitude(Double.parseDouble(temp[1]));
		aILoc.setAccuracy(Float.parseFloat(temp[2]));	//Radius, proximity
		isInverted = Integer.parseInt(temp[3]);	//isInverted meaning at any place BUT this!
		Log.i("GPSCheckService -> isSatisfied",  l.getAccuracy() + " "+l.getLongitude()+" "+l.getLatitude());
		Log.i("GPSCheckService -> isSatisfied", isInverted +" "+ " "+aILoc.getAccuracy()+" "+aILoc.getLongitude()+" "+aILoc.getLatitude());
		float distance = l.distanceTo(aILoc);
		Log.i("GPSCheckService -> isSatisfied", "distance: "+distance);
		if(distance<=aILoc.getAccuracy()+l.getAccuracy()){		//Check if l get accuracy
			Log.i("GPSCheckService -> isSatisfied","Seems you're close to "+aI.title);
			if(l.getAccuracy()>50 && moreAccurateDataSet==0){
				Log.i("GPSCheckService -> isSatisfied","Your location is less accurate");
				if(distance<=aILoc.getAccuracy()+50){
					Log.i("GPSCheckService -> isSatisfied","In range. Will be inrange even if more accurate");
					//AutomationHandler.checkOtherTriggers(getApplicationContext(),aI, AutomationItem.LOCATION_TRIGGER);
					if(isInverted==0) return true;
					else return false;
				}
				else{
					//GET MORE ACCURATE DATA!!
					Log.i("GPSCheckService -> isSatisfied", "Need more accurate data");
					stopService(serviceIntent);
					moreAccurateDataSet=1;
					getGPSServiceData(50, 1, 1, 30000);
					if(isInverted==0) return false;
					else return true;
				}
			}
			else{
				Log.i("GPSCheckService -> isSatisfied","MoreAccurateData "+moreAccurateDataSet);
				Log.i("GPSCheckService -> isSatisfied","In range.");
				//AutomationHandler.checkOtherTriggers(getApplicationContext(),aI, AutomationItem.LOCATION_TRIGGER);
				if(isInverted==0) return true;
				else return false;
			}
		}
		else{
			Log.i("GPSCheckService -> isSatisfied","Not in range of "+aI.title);
			if(isInverted==0) return false;
			else return true;
		}
	}
	
	
	
	
	private void checkTodo(Location l){				//passing my location
		if(todoList==null){
			String fileDir=callingIntent.getExtras().getString("FILEDIR");
			if(fileDir==null)	Log.e("GPSCheckService -> checkTodo", "FILEDIR was not found in intent! "+fileDir);
			todoList = TodoFileProcessor.getTodos(fileDir);	
			Log.i("GPSCheckService -> checkTodo", "List populated");
		}
		Log.i("GPSCheckService -> checkTodo", "Todo List Size "+todoList.size());
		for(TodoItem todo : todoList){
			Log.i("GPSCheckService -> checkTodo", "Checking todo item"+todo.title);
			
			Location todoLoc = new Location("aILoc");
			todoLoc.setLongitude(todo.longitude);
			todoLoc.setLatitude(todo.latitude);
			todoLoc.setAccuracy(todo.radius);			//Radius, proximity
		
			float distance = l.distanceTo(todoLoc);
			if(distance<=todoLoc.getAccuracy()+l.getAccuracy()){		//Check if l get accuracy
				Log.i("GPSCheckService","Seems you're close to "+todo.title);
				if(l.getAccuracy()>50 && moreAccurateDataSet==0){
					Log.i("GPSCheckService","Your location is less accurate");
					if(distance<=todoLoc.getAccuracy()+50){
						Log.i("GPSCheckService","Giving alert!");
					
						todoAlertHandler(todo, true);
			
					}
					else{
						//GET MORE ACCURATE DATA!!
						Log.i("GPSCheckService", "Need more accurate data");
						stopService(serviceIntent);
						moreAccurateDataSet=1;
						getGPSServiceData(50, 1, 1, 30000);
					}
				}
				else{
					Log.i("GPSCheckService","MoreAccurateData "+moreAccurateDataSet);
					Log.i("GPSCheckService","Giving alert!");
					
					todoAlertHandler(todo, true);
					//TodoFileProcessor.deleteTodo(getApplicationContext(), todo);
					todo.justAlerted=true;
					//TodoFileProcessor.writeTodo(getApplicationContext(), todo);
				}
			}
			else{
				Log.i("GPSCheckService","Not in range of "+todo.title);
				//TodoFileProcessor.deleteTodo(getApplicationContext(), todo);
				todo.justAlerted=false;
				todoAlertHandler(todo, false);

				//TodoFileProcessor.writeTodo(getApplicationContext(), todo);
			}
		}
		
	}
	
	public void todoAlertHandler(TodoItem todo, boolean update){
		HashMap todoAlertsStatus ;
		boolean status;
		todoAlertsStatus =	AlertsFileProcessor.getTodoAlertStatus(""+getApplicationContext().getExternalFilesDir(null));
		Log.i("GPSCheckService:todoAlertHandler", "HashMap size: "+todoAlertsStatus.size());
		Object previousStatus = todoAlertsStatus.put(todo.sno1, update);
		
		if(previousStatus==null){
			Log.i("GPSCheckService:todoAlertHandler", "No previous record in AlertStatusFile");
		}
		
		if((previousStatus!=null) && ((Boolean) previousStatus == update)){
			Log.i("GPSCheckService:todoAlertHandler", "No alerts or updates!");
			return;		//No updates & No alert!
		}
		else{
			if(update){
				Log.i("GPSCheckService:todoAlertHandler", "Alert! will update too");
				//SHOW ALERT!
				Intent intent = new Intent("com.spawnalert");
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				intent.putExtra("Todo", todo);
				intent.putExtra("Type", "GPSCheckService"); 	//0 - GPS trigger
				sendBroadcast(intent);
			}
			else{
				Log.i("GPSCheckService:todoAlertHandler", "only update");
			}
			AlertsFileProcessor.writeTodoAlertStatus(""+getApplicationContext().getExternalFilesDir(null), todoAlertsStatus);
		}
	}
	
	private void getGPSServiceData(float reqAccuracy, int reqDatasets, int maxTotalDataset, int GPStimeout) {
		serviceIntent = new Intent(this, GPSService.class);
		// Create a new Messenger for the communication back
		Messenger messenger = new Messenger(handler);
		serviceIntent.putExtra("MESSENGER", messenger);
		serviceIntent.putExtra("reqAccuracy", reqAccuracy);
		serviceIntent.putExtra("reqDatasets", reqDatasets);
		serviceIntent.putExtra("maxTotalDataset",maxTotalDataset);
		serviceIntent.putExtra("GPStimeout", GPStimeout);
		startService(serviceIntent);
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.i("GPSCheckService", "Started");
		callingIntent=intent;
		getGPSServiceData(500, 1, 1, 50000);
		super.onStartCommand(intent, flags, startId);
        return START_NOT_STICKY;			///CONSIDER CHANGE -- TEST!!!
	}
	
	
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
	}
	
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void onDestroy() {
		Log.i("GPSCheckService","life lived. destroy!");
		if(serviceIntent!=null) stopService(serviceIntent);
		super.onDestroy();
	}

	
}
