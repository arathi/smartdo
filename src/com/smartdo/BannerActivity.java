package com.smartdo;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;

import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class BannerActivity extends Activity {
	private AdView adView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_banner);
		
		AdView adView = (AdView)this.findViewById(R.id.adView);
        AdRequest re = new AdRequest();
       // re.setTesting(true);
        adView.loadAd(re);

	    // Lookup your LinearLayout assuming it's been given
	    // the attribute android:id="@+id/mainLayout"
	    //RelativeLayout layout = (RelativeLayout)findViewById(R.id.adLayout);

	    // Add the adView to it
	    //layout.addView(adView);
	    
	    // Initiate a generic request to load it with an ad
	    ///adView.loadAd(new AdRequest());
	    //replaced by
//		(new Thread() {
//            public void run() {
//                 Looper.prepare();
//                 AdRequest aR = new AdRequest();
//                 aR.addTestDevice("7A0C5219D48AF532FE58346F914C1C9E");
//                adView.loadAd(aR);
//
//            }
//        }).start();
	    
	    
	  }

	  @Override
	  public void onDestroy() {
	    if (adView != null) {
	      adView.destroy();
	    }
	    super.onDestroy();
	  }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_banner, menu);
		return true;
	}

}
