package com.smartdo;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import com.smartdo.files.TodoFileProcessor;

/**
 * Broadcast receiver for reading a SMS message on receipt and doing operations accordingly if it's a SMARTDO message.
 * 
 * @author aref
 *
 */

public class SmsReader extends BroadcastReceiver {
	// private static final String TAG = "Message recieved";

	@Override
	public void onReceive(Context context, Intent intent) {
		Bundle pudsBundle = intent.getExtras();
		Object[] pdus = (Object[]) pudsBundle.get("pdus");
		SmsMessage messages = SmsMessage.createFromPdu((byte[]) pdus[0]);
		// Log.i(TAG, messages.getMessageBody());
		// Toast.makeText(context, "SMS Received : "+messages.getMessageBody()
		// +"From : " + messages.getDisplayOriginatingAddress(),
		// Toast.LENGTH_LONG).show();

		String smsBody = messages.getMessageBody();
		String[] smsComp = smsBody.split("_");

		// Toast.makeText(context, smsComp[0], Toast.LENGTH_SHORT).show();
		Log.i("SMS Parsing..", smsComp[0]);

		int sno1;
		int sno2;
		ArrayList<TodoItem> al;
		Iterator<TodoItem> it;
		
		if (smsComp[0].equals("$") && smsComp[1].equals("SMARTDO")) {
			abortBroadcast(); // Stop all other broadcast receivers

			Toast.makeText(context, "SMARTDO message consumed",
					Toast.LENGTH_SHORT).show();
			switch (Integer.parseInt(smsComp[2])) {
			case 1:
				final TodoItem temp = new TodoItem();
				temp.setAuthor(messages.getDisplayOriginatingAddress());
				temp.setAssigned_to("me");
				temp.setSno1(Integer.parseInt(smsComp[3]));
				temp.setSno2(Integer.parseInt(smsComp[4]));
				temp.setTitle(smsComp[5]);
				temp.setDue_date(new Date(Long.parseLong(smsComp[6])));
				temp.setLatitude(Double.parseDouble(smsComp[7]));
				temp.setLongitude(Double.parseDouble(smsComp[8]));
				temp.setRadius(Float.parseFloat(smsComp[9]));
				temp.setPriority(Integer.parseInt(smsComp[10]));

				Log.i("Rece TODO ITEM:",
						"" + temp.getSno1() + temp.getSno2() + temp.getTitle()
								+ temp.getAssigned_to() + temp.getAuthor()
								+ temp.getLatitude() + temp.getLongitude()
								+ temp.getPriority() + temp.getRadius()
								+ temp.getDue_date());
				
				Intent i = new Intent(context, SharingAlert.class);
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				i.putExtra("todo", temp);
				context.startActivity(i);
				
				
				break;
			
			case 2:
				sno1=Integer.parseInt(smsComp[3]);
				sno2=Integer.parseInt(smsComp[4]);
				boolean isAccepted = Boolean.parseBoolean(smsComp[5]);
				
				//find the todo
				al = TodoFileProcessor.getTodos(context);
				it = al.iterator();
				
				while(it.hasNext()){
					TodoItem todo=it.next();
					if(todo.sno1==sno1 && todo.sno2==sno2){
						Log.i("SMSBR", "Found the todo");
						if(isAccepted){
							TodoFileProcessor.deleteTodo(context, todo);
							todo.isConfirmed=true;
							TodoFileProcessor.writeTodo(context, todo);
							Log.i("SMSBR", "Show confirmation");
							NotifiManager.notify(context, "Shared Task Approved",todo.title, 32);
						}
						else{
							Log.i("SMSBR", "Showing regret notificaition");
							NotifiManager.notify(context, "Shared Task Rejected",todo.title, 32);
						}
					}
				}
				break;
			case 3:
				sno1=Integer.parseInt(smsComp[3]);
				sno2=Integer.parseInt(smsComp[4]);
				
				//find the todo
				al = TodoFileProcessor.getTodos(context);
				it = al.iterator();
				
				while(it.hasNext()){
					TodoItem todo=it.next();
					if(todo.sno1==sno1 && todo.sno2==sno2){
						Log.i("SMSBR", "Found the todo");
						Log.i("SMSBR", "Showing regret notificaition");
						NotifiManager.notify(context, "Shared Task Completed!",todo.title, 32);
						TodoFileProcessor.deleteTodo(context, todo);
					}
				}
				break;
			}
		}
	}
}
