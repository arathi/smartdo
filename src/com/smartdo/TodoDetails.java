package com.smartdo;

import java.io.File;

import org.mapsforge.android.maps.MapActivity;
import org.mapsforge.android.maps.MapController;
import org.mapsforge.android.maps.MapView;
import org.mapsforge.android.maps.overlay.ArrayCircleOverlay;
import org.mapsforge.android.maps.overlay.ItemizedOverlay;
import org.mapsforge.android.maps.overlay.OverlayCircle;
import org.mapsforge.android.maps.overlay.OverlayItem;
import org.mapsforge.core.GeoPoint;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Messenger;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;
 /**
  * 
  * @author Amrit
  * This class is used to show the display the details of a TodoItem
  *
  */
public class TodoDetails extends MapActivity {
	MapView map;
	Context context = this;
	Intent serviceIntent;

	@Override
	/**
	 * @author Amrit
	 * This class inflates the map view, sets the map file and
	 * displays the todo location on the map
	 */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_todo_details);
		TextView todoTitle = (TextView) findViewById(R.id.title_string);
		TextView todoDate = (TextView) findViewById(R.id.date_string);
		TodoItem item = (TodoItem) getIntent().getSerializableExtra("todo");
		
		// Log.i("TODO ISCONFIRMED",""+item.isConfirmed);
		
		todoTitle.setText(item.title);
		todoDate.setText(item.due_date.toString());
		GeoPoint geoPoint=new GeoPoint(item.latitude, item.longitude);
		
		map = (MapView) findViewById(R.id.mapView);
		map.setMapFile(new File("/mnt/sdcard/Indore.map"));
		map.setClickable(true);
		map.setBuiltInZoomControls(true);
		map.isFocusable();
		
		MapController mc=map.getController();
		mc.setZoom(15);
		
		getGPSServiceData();
		
		Log.d("todo", item.title + "-" + item.due_date.toString() + "-"
				+ item.latitude + "-" + item.longitude);
		
		Drawable defaultMarker = getResources()
				.getDrawable(R.drawable.mapmaker);
		ArrayItemizedOverlay arr = new ArrayItemizedOverlay(defaultMarker, this);
		OverlayItem temp = new OverlayItem(geoPoint, item.title, "",
				ItemizedOverlay.boundCenterBottom(defaultMarker));
		temp.setMarker(defaultMarker);
		map.setCenter(geoPoint);
		arr.addOverlay(new OverlayItem(geoPoint, item.title, ""));
		map.getOverlays().add(arr);
		Paint fillPaint=new Paint();
		fillPaint.setColor(Color.BLUE);
		Paint outlinePaint=new Paint();
		outlinePaint.setColor(Color.RED);
		fillPaint.setAlpha(30);
		
		OverlayCircle temp1=new OverlayCircle(geoPoint, item.radius, fillPaint, null, item.title);
		ArrayCircleOverlay arrCircle=new ArrayCircleOverlay(fillPaint, outlinePaint);
		arrCircle.addCircle(temp1);
		arr.addOverlay(temp);
		map.getOverlays().add(arr);
		map.getOverlays().add(arrCircle);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_todo_details, menu);
		return true;
	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			Location l = (Location) msg.obj;
			if (l != null) {
				ArrayItemizedOverlay arr1;
				Drawable myPositionMarker = getResources().getDrawable(
						R.drawable.pegman);
				arr1 = new ArrayItemizedOverlay(myPositionMarker, context);
				Toast.makeText(
						context,
						l.getLatitude() + "," + l.getLongitude() + ","
								+ l.getAccuracy(), Toast.LENGTH_SHORT).show();
				GeoPoint mylocation = new GeoPoint(l.getLatitude(),
						l.getLongitude());
				OverlayItem mylocationItem = new OverlayItem(mylocation,
						"My Location", "");
				arr1.addOverlay(mylocationItem);
				map.getOverlays().add(arr1);
			} else {
				Toast.makeText(context,
						"Location object returned from service is null.",
						Toast.LENGTH_SHORT).show();
			}
		};
	};
	/**
	 * @author Aref
	 * This method gets the current location of the user
	 */
	private void getGPSServiceData() {
		serviceIntent = new Intent(this, GPSService.class);
		// Create a new Messenger for the communication back
		Messenger messenger = new Messenger(handler);
		serviceIntent.putExtra("MESSENGER", messenger);
		serviceIntent.putExtra("reqAccuracy", 20f);
		serviceIntent.putExtra("reqDatasets", 1);
		startService(serviceIntent);
	}

	@Override
	protected void onPause() {
		if (serviceIntent != null)
			stopService(serviceIntent);
		super.onPause();
	}

}
