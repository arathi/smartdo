package com.smartdo;

import java.util.ArrayList;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class BookmarksAdapter extends ArrayAdapter<BookmarkItem> {
	private Activity context;
	ArrayList<BookmarkItem> bookmarks;
	
	public BookmarksAdapter (Activity context, int resource, ArrayList<BookmarkItem> bookmarks){
		super(context, resource, bookmarks);
		this.context=context;
		this.bookmarks=bookmarks;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		return super.getView(position, convertView, parent);
	}
	
}
