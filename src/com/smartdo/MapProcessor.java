package com.smartdo;

import java.io.File;
import java.util.ArrayList;

import org.mapsforge.android.maps.MapActivity;
import org.mapsforge.android.maps.MapView;
import org.mapsforge.android.maps.overlay.ArrayCircleOverlay;
import org.mapsforge.android.maps.overlay.ItemizedOverlay;
import org.mapsforge.android.maps.overlay.OverlayCircle;
import org.mapsforge.android.maps.overlay.OverlayItem;
import org.mapsforge.core.GeoPoint;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Messenger;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.Toast;

import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.smartdo.files.BookmarkFileProcessor;
import com.smartdo.files.TodoFileProcessor;
/**
 * This class extends MapActivity and shows maps according to the conditions.
 * It can be used according to the action field specified in the action String.
 * It overrides various methods and shows the map with Overlays of Todos and Bookmarks
 * @author Amrit
 * @author Aref
 * @author Prerna
 * @author Rashi
 *
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class MapProcessor extends MapActivity {
	Intent serviceIntent;
	public static float[] point = new float[2];
	static GeoPoint p;
	Drawable defaultMarker = null;
	ArrayItemizedOverlay arr = null;
	MapView mapview = null;
	String action;
	Context context = this;
	GeoPoint mylocation;
	OverlayItem mylocationItem;
	OverlayCircle myPositionCircled;
	ArrayCircleOverlay arrCircle;
	Drawable myPositionMarker;
	AdView adView;
	AdRequest re;
	Button bookmark;
	String flingdir="";
	public static String caller="";
	private GestureDetector gestureDetector;
	Boolean gpsCheck=false;
	Boolean isLoadedPreviously=false;
	
	@Override
	/**
	 * 
	 * @author Amrit
	 * @author Aref
	 * @author Prerna
	 * @author Rashi
	 *
	 */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		BookmarkFileProcessor.getBookmarks(this);
		TodoFileProcessor.getTodos(this);

		String anim = ""+getIntent().getStringExtra("anim");
		p=new GeoPoint(0, 0);
		if(!isLoadedPreviously){
			if(anim.equals("left")){
				flingdir="left";
				overridePendingTransition(R.anim.rightflingin,R.anim.rightflingout);
			}
			else if (anim.equals("right")){
				flingdir="right";
				overridePendingTransition(R.anim.leftflingin,R.anim.leftflingout);
			}
		}
		else{
			overridePendingTransition(0, 0);
		}
		caller = ""+getIntent().getStringExtra("caller");
		isLoadedPreviously=true;
		gestureDetector = new GestureDetector(
                new SimpleOnGestureListener(){
                	 private static final int SWIPE_MIN_DISTANCE = 120;
                	    private static final int SWIPE_MAX_OFF_PATH = 200;
                	    private static final int SWIPE_THRESHOLD_VELOCITY = 200;

                	    @Override
                	    public boolean onFling(MotionEvent e1, MotionEvent e2,
                	                         float velocityX, float velocityY) {
                	      try {
                	        float diffAbs = Math.abs(e1.getY() - e2.getY());
                	        float diff = e1.getX() - e2.getX();

                	        if (diffAbs > SWIPE_MAX_OFF_PATH)
                	          return false;
                	        
                	        // Left swipe
                	        if (diff > SWIPE_MIN_DISTANCE
                	        && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
	                 	           //showMap(null);
                	        	finish();


                	        // Right swipe
                	        } else if (-diff > SWIPE_MIN_DISTANCE
                	        && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                	        	//showTodo(null);
                	        	finish();

                	        }
                	      } catch (Exception e) {
                	        Log.e("MainActivity", "Error on gestures");
                	      }
                	      return false;
                	    }
                	  }
                );
		
		
		
		setContentView(R.layout.map_layout);
		action = getIntent().getStringExtra("action");
		// action=overlays, todo, automation, bookmark
		mapview = (MapView) findViewById(R.id.mapView1);
		if (mapview == null) {
			Log.d("maps", "view not found");
		}
		mapview.setClickable(true);
		mapview.setBuiltInZoomControls(true);
		mapview.setMapFile(new File("mnt/sdcard/Indore.map"));
		mapview.isFocusable();
		defaultMarker = getResources().getDrawable(R.drawable.mapmaker);
		arr = new ArrayItemizedOverlay(null, this);
		arrCircle=new ArrayCircleOverlay(null, null);
		myPositionMarker = getResources().getDrawable(R.drawable.pegman);
		
		mapview.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {

				p = ((MapView) arg0).getProjection().fromPixels(
						(int) arg1.getX(), (int) arg1.getY());
				// Toast.makeText(context,
				// p.getLatitude() + "-on map-" + p.getLongitude(),
				// Toast.LENGTH_SHORT).show();
				return false;
			}
		});
		bookmark = (Button) findViewById(R.id.addBookmarkButton);
		if (action.equals("overlays")) {
			drawOverlays(mapview);
		} else if (action.equals("todo") || (action.equals("automation"))) {
			getLocationCoord("Select Location");
		} else if (action.equals("bookmark")) {
			viewAddBookmark();
		}
		
		adView = (AdView) this.findViewById(R.id.adView);
		re = new AdRequest();
	}

	public float[] getPoints(GeoPoint p) {
		point[0] = (float) p.getLatitude();
		point[1] = (float) p.getLatitude();
		return point;
	}

	/**
	 * @author Amrit
	 * @author Rashi
	 * @author Prerna
	 * @param mapview
	 * 
	 */
	public void drawOverlays(MapView mapview) {
		bookmark.setText("Save Bookmark");
		bookmark.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(context, AddBookmark.class);
				if(p.getLatitude()==0){
					Toast.makeText(context, "Please Select a location first!", Toast.LENGTH_SHORT).show();
				}
				else{
					intent.putExtra("lat", p.getLatitude());
					intent.putExtra("long", p.getLongitude());
					startActivityForResult(intent, 1001);
				}
			}
		});
		ArrayList<TodoItem> todoItems = TodoFileProcessor.todoItems;
		ArrayList<BookmarkItem> bookmarkItems = BookmarkFileProcessor.bookmarkItems;
		if (todoItems != null) {
			for (int i = 0; i < todoItems.size(); i++) {
				String title = todoItems.get(i).getTitle();
				Double lat = todoItems.get(i).getLatitude();
				Double lon = todoItems.get(i).getLongitude();
				GeoPoint geoPoint = new GeoPoint(lat, lon);
				OverlayItem temp = new OverlayItem(geoPoint, title, "",
				ItemizedOverlay.boundCenterBottom(defaultMarker));
				temp.setMarker(defaultMarker);
				arr.addOverlay(temp);
				mapview.getOverlays().add(arr);
				Paint fillPaint=new Paint();
				fillPaint.setColor(Color.BLUE);
				Paint outlinePaint=new Paint();
				outlinePaint.setColor(Color.RED);
				fillPaint.setAlpha(30);
				
				OverlayCircle temp1=new OverlayCircle(geoPoint, todoItems.get(i).radius, fillPaint, null, title);
				
				arrCircle.addCircle(temp1);
				arr.addOverlay(temp);
				mapview.getOverlays().add(arr);
				mapview.getOverlays().add(arrCircle);
				Log.d("drawing", lat + " " + lon);
			}
		}
		if (bookmarkItems != null) {
			for (int i = 0; i < bookmarkItems.size(); i++) {
				String title = bookmarkItems.get(i).getTitle();
				Double lat = bookmarkItems.get(i).getLatitude();
				Double lon = bookmarkItems.get(i).getLongitude();
				GeoPoint geoPoint = new GeoPoint(lat, lon);
				OverlayItem temp = new OverlayItem(geoPoint, title, "",
						ItemizedOverlay.boundCenterBottom(defaultMarker));
				arr.addOverlay(temp);
				mapview.getOverlays().add(arr);
			}
		}
	}

	public void viewAddBookmark() {

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		Log.i("motion", "yup");
	    if (gestureDetector.onTouchEvent(event)) {
	      return true;
	    }
	    return super.onTouchEvent(event);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Log.d("back to map", "true");
		finish();
		startActivity(getIntent());
	}

	public void getLocationCoord(String str) {
		Button todo = (Button) findViewById(R.id.addBookmarkButton);
		todo.setText(str);
		todo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if(p.getLatitude()==0){
					Toast.makeText(context, "Please select a location on map", Toast.LENGTH_SHORT).show();
				}
				else{
					Intent data = new Intent();
					data.putExtra("lat", p.getLatitude());
					data.putExtra("longi", p.getLongitude());
					setResult(5001, data); //send data back to parent
					finish();
				}
			}
		});
	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			Location l = (Location) msg.obj;
			if (l != null) {
				arr.removeOverlay(mylocationItem);
				Toast.makeText(
						context,
						l.getLatitude() + "," + l.getLongitude() + ","
								+ l.getAccuracy(), Toast.LENGTH_SHORT).show();
				mylocation = new GeoPoint(l.getLatitude(), l.getLongitude());
				mylocationItem = new OverlayItem(mylocation, "My Location", "",
						ItemizedOverlay.boundCenterBottom(myPositionMarker));
				arr.addOverlay(mylocationItem);
				mapview.getOverlays().add(arr);
				mapview.setCenter(mylocation);
				re.setLocation(l);
				adView.loadAd(re);

			} else {
				Toast.makeText(context,
						"Location object returned from service is null.",
						Toast.LENGTH_SHORT).show();

			}
			Button b = (Button) findViewById(R.id.gpsButton);
			b.setActivated(false);
		};
	};
	
	public void getGPSServiceData(View view) {
		serviceIntent = new Intent(this, GPSService.class);
		// Create a new Messenger for the communication back
		Messenger messenger = new Messenger(handler);
		serviceIntent.putExtra("MESSENGER", messenger);
		serviceIntent.putExtra("reqAccuracy", 50f);
		serviceIntent.putExtra("reqDatasets", 1);
		serviceIntent.putExtra("maxTotalDataset", 2);
		serviceIntent.putExtra("GPStimeout", 30000);
		startService(serviceIntent);
		Button b = (Button) findViewById(R.id.gpsButton);
		b.setActivated(true);
	}
	@Override
	public void finish() {
		if(serviceIntent!=null){
			stopService(serviceIntent);
		}
		super.finish();
		if(flingdir.equals("left"))
			overridePendingTransition(R.anim.leftflingin,R.anim.leftflingout);
		else if (flingdir.equals("right"))
			overridePendingTransition(R.anim.rightflingin, R.anim.rightflingout);
		else
			overridePendingTransition(0,0);

	}
}
