package com.smartdo;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationManager;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.util.Log;
import android.widget.Toast;

import com.smartdo.files.AutoFileProcessor;
/**
 * Handles all back-end operations of Automation Items. 
 * It checks for trigger satisfaction in multiple circumstances, it does the actions, and also some other utility functions.
 * 
 * @author aref
 *
 */
public class AutomationHandler {

	public static void printAutomationTask(AutomationItem ai){
		Log.i("AddingAutomation", "TRIGGERS");
		Log.i("Ringer",
				"" + ai.triggers.get(AutomationItem.RINGER_MODE_TRIGGER));
		Log.i("ICall",
				"" + ai.triggers.get(AutomationItem.INCOMING_CALL_TRIGGER));
		Log.i("LBattery",
				"" + ai.triggers.get(AutomationItem.LOW_BATTERY_TRIGGER));
		Log.i("ULock", "" + ai.triggers.get(AutomationItem.UNLOCK_TRIGGER));
		Log.i("Alarm", "" + ai.triggers.get(AutomationItem.ALARM_TRIGGER));
		Log.i("GPS", "" + ai.triggers.get(AutomationItem.LOCATION_TRIGGER));

		Log.i("AddingAutomation->", "ACTIONS:");
		Log.i("Ringer", "" + ai.actions.get(AutomationItem.RINGER_MODE_ACTION));
		Log.i("Bluetooth", "" + ai.actions.get(AutomationItem.BLUETOOTH_ACTION));
		Log.i("Data", "" + ai.actions.get(AutomationItem.DATA_ACTION));
		Log.i("WIFI", "" + ai.actions.get(AutomationItem.WIFI_ACTION));
		Log.i("Brightness",
				"" + ai.actions.get(AutomationItem.SCREEN_BRIGHTNESS_ACTION));
		Log.i("SMSAction", " "+ ai.actions.get(AutomationItem.SMS_ACTION));
		Log.i("PrintAutomation","upto here!");
	}
	
	
	/*
	 * 
	 * TRIGGERS
	 * 
	 */

	public static void addAutomationTask(Context context, AutomationItem ai) {
		printAutomationTask(ai);
		if (ai.triggers.get(AutomationItem.INCOMING_CALL_TRIGGER) != null) {
			// TODO - v2 //nothing as dependent on br!?
			Log.i("AutomationHandler", "Incoming BR will handle");
			return;

		}
		if (ai.triggers.get(AutomationItem.UNLOCK_TRIGGER) != null) {
			// TODO - v2 //nothing as dependent on br
			Log.i("AutomationHandler", "UserPresentReceiver will handle");

			return;
		}
		if (ai.triggers.get(AutomationItem.LOCATION_TRIGGER) != null) {
			// No action required as it'll be checked by GPSCheckService
			Log.i("AutomationHandler", "GPSCheckService will handle");
			return;
		}
		if (ai.triggers.get(AutomationItem.ALARM_TRIGGER) != null) {
			Log.i("AutomationHandler", "Inserted into Alarm Manager");
			AlaManager.addAlarm(context, ai);
			return;
		}
		if (ai.triggers.get(AutomationItem.RINGER_MODE_TRIGGER) != null) {
			// TODO - v2 //nothing as dependent on br!?
		}

		if (ai.triggers.get(AutomationItem.LOW_BATTERY_TRIGGER) != null) {
			// TODO //nothing as dependent on br!?
			// INSERT INTO THE LOW BATTERY CHECKING FILE
		}

		Log.i("AutomationHandler", "No triggers found!");
	}

	public static void checkOtherTriggers(Context context, AutomationItem aI, int callerID) {
		int caller = callerID;
		if(caller == -2){
			caller=AutomationItem.LOCATION_TRIGGER;	//to make it check second if
			//For other BR triggers. Will check location too!
			if(aI.triggers.containsKey(AutomationItem.LOCATION_TRIGGER)){
				LocationManager locationManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
				Location temp =locationManager.getLastKnownLocation(locationManager.GPS_PROVIDER);
				Log.i("AutomationHandler->checkOtherTriggers","checking location");
				GPSCheckService gcs = new GPSCheckService();
				if(!gcs.isSatisfied(aI, temp)){
					Log.i("AutomationHandler->checkOtherTriggers", "location failed");
					return;
				}
			}
		}
		
		if(caller==AutomationItem.LOCATION_TRIGGER){
//			Log.i("temp","second if");
			//Meaning called after location confirmation, requires all to be checked!
			//Checking time
			
			if(aI.triggers.containsKey(AutomationItem.ALARM_TRIGGER)){
				Log.i("AutomationHandler->checkOtherTriggers", "Location OK, checking ALARM_TRIGGER");
				Long aTime = Long.parseLong(aI.triggers.get(AutomationItem.ALARM_TRIGGER));
				Long cTime = System.currentTimeMillis();
				Long margin = Long.parseLong(aI.triggers.get(AutomationItem.ALARM_TRIGGER_MARGIN));
//				Log.i("Atime",""+aTime);
//				Log.i("Ctime",""+cTime);
//				Log.i("Margin",""+margin);
				if(!(cTime-aTime<=margin && cTime-aTime>0)){	//Update 22/5/13 - bug fix
					Log.i("AutomationHandler->checkOtherTriggers", "ALARM_TRIGGER failed");
					return;			//alarm trigger fail
				}	
				Log.i("AutomationHandler->checkOtherTriggers", "ALARM_TRIGGER Satisfied");
			}
		}//if it misses first if, means it's an alarm trigger.. hence we need to check triggers other than alarm	

		//LOW BATTERY CHECK
		if(aI.triggers.containsKey(AutomationItem.LOW_BATTERY_TRIGGER)){
			Log.i("AutomationHandler->checkOtherTriggers", "LOW_BATTERY_TRIGGER");
			if(!(isBatteryLow(context, Integer.parseInt(aI.triggers.get(AutomationItem.LOW_BATTERY_TRIGGER))))){
				Log.i("AutomationHandler->checkOtherTriggers", "LOW_BATTERY_TRIGGER failed");
				return;	//if battery is not low
			}
		}

		//RINGER MODE CHECK
		if(aI.triggers.containsKey(AutomationItem.RINGER_MODE_TRIGGER)){
			Log.i("AutomationHandler->checkOtherTriggers", "RINGER_MODE_TRIGGER");
			int tobe = Integer.parseInt(aI.triggers.get(AutomationItem.RINGER_MODE_TRIGGER));
			if(!(isRingerMode(context, tobe))){
				Log.i("AutomationHandler->checkOtherTriggers", "RINGER_MODE_TRIGGER failed");
				return;	//ringer mode not same
			}
		}
		doActions(context, aI);
	}
	
	public static boolean isBatteryLow(Context context, int lowlevel){
		IntentFilter filter=new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
		Intent batt=context.getApplicationContext().registerReceiver(null,filter);
		int level = 99; // Default to some unknown/wild value
		int scale=100;
		float percent = -1;
		// registerReceiver method call could return null, so check that!
		if (batt != null) {
		    level = batt.getIntExtra(BatteryManager.EXTRA_LEVEL, 99);
		    scale = batt.getIntExtra(BatteryManager.EXTRA_SCALE, 100);
		    percent = ((float)level/scale)*100;
		}  
		//Toast.makeText(context, "Battery Percent " +percent, Toast.LENGTH_SHORT).show();
		if(percent!=-1 && (percent<lowlevel)){
			return true;
		}
		return false;
	}
	
	public static boolean isRingerMode(Context context, int tobe){
		Log.i("To check with:",""+tobe);
		AudioManager am = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
		Log.i("Found to be:",""+am.getRingerMode());
		if(tobe == am.getRingerMode()){
			return true;
		}
		return false;
	}

	/*
	 * 
	 * ACTIONS
	 */

	public static void doActions(Context context, AutomationItem ai) {
		Log.i("AutomationHandler->doActions","performing actions!");
		/*Log.i("AutomationHandler->", "doActions");
		Log.i("AutomationHandler->Ringer", "" + ai.actions.get(AutomationItem.RINGER_MODE_ACTION));
		Log.i("AutomationHandler->Bluetooth", "" + ai.actions.get(AutomationItem.BLUETOOTH_ACTION));
		Log.i("AutomationHandler->Data", "" + ai.actions.get(AutomationItem.DATA_ACTION));
		Log.i("AutomationHandler->WIFI", "" + ai.actions.get(AutomationItem.WIFI_ACTION));
		Log.i("AutomationHandler->Brightness",
				"" + ai.actions.get(AutomationItem.SCREEN_BRIGHTNESS_ACTION));
		Log.i("AutomationHandler->SMS", "" + ai.actions.get(AutomationItem.SMS_ACTION));
		 */
		if (ai.actions.get(AutomationItem.WIFI_ACTION) != null) {
			doWifiAction(context, ai.actions.get(AutomationItem.WIFI_ACTION));
		}
		if (ai.actions.get(AutomationItem.DATA_ACTION) != null) {
			doDataAction(context, ai.actions.get(AutomationItem.DATA_ACTION));
		}
		if (ai.actions.get(AutomationItem.BLUETOOTH_ACTION) != null) {
			doBluetoothAction(context,
					ai.actions.get(AutomationItem.BLUETOOTH_ACTION));
		}
		if (ai.actions.get(AutomationItem.RINGER_MODE_ACTION) != null) {
			doRingerAction(context,
					ai.actions.get(AutomationItem.RINGER_MODE_ACTION));
		}
		if (ai.actions.get(AutomationItem.SCREEN_BRIGHTNESS_ACTION) != null) {
			// TODO
		}
		if (ai.actions.get(AutomationItem.SMS_ACTION) != null) {
			doSMSAction(context, ai.actions.get(AutomationItem.SMS_ACTION));
		}

	}

	private static void doWifiAction(Context context, String string) {
		WifiManager wifi;
		wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

		if (string.equals("" + AutomationItem.TURN_OFF) && wifi.isWifiEnabled()) {
			wifi.setWifiEnabled(false);
		} else if (string.equals("" + AutomationItem.TURN_ON)
				&& !wifi.isWifiEnabled()) {
			wifi.setWifiEnabled(true);
		}
	}

	private static void doDataAction(Context context, String string) {
		if (/* isDataEnabled(context) && */string.equals(""
				+ AutomationItem.TURN_OFF)) {
			updateData(context, false);
		} else if (/* isDataEnabled(context) && */string.equals(""
				+ AutomationItem.TURN_ON)) {
			updateData(context, true);
		}
	}

	private static void updateData(Context paramContext, boolean enable) {
		try {
			ConnectivityManager connectivityManager = (ConnectivityManager) paramContext
					.getSystemService("connectivity");
			Method setMobileDataEnabledMethod = ConnectivityManager.class
					.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
			setMobileDataEnabledMethod.setAccessible(true);
			setMobileDataEnabledMethod.invoke(connectivityManager, enable);
		} catch (Exception e) {
			Toast.makeText(paramContext,
					"AutomationHandler->Exception in updateData - " + e.toString(),
					Toast.LENGTH_SHORT).show();
			e.printStackTrace();
		}
	}

	// private static boolean isDataEnabled(Context paramContext) {
	// try {
	// NetworkInfo networkInfo = ((ConnectivityManager)
	// paramContext.getSystemService("connectivity")).getActiveNetworkInfo();
	// boolean toReturn = networkInfo.isConnectedOrConnecting();
	// return false;
	// } catch (Exception e) {
	// Toast.makeText(paramContext,
	// "Exception in isDataEnabled - "+e.toString(), Toast.LENGTH_SHORT).show();
	// return false;
	// }
	// }

	private static void doBluetoothAction(Context context, String string) {
		BluetoothAdapter mBluetoothAdapter = BluetoothAdapter
				.getDefaultAdapter();
		if (!mBluetoothAdapter.isEnabled()
				&& string.equals("" + AutomationItem.TURN_ON)) {
			mBluetoothAdapter.enable();
		} else if (mBluetoothAdapter.isEnabled()
				&& string.equals("" + AutomationItem.TURN_OFF)) {
			mBluetoothAdapter.disable();
		}
	}

	private static void doRingerAction(Context context, String string) {
		AudioManager am;
		am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

		switch (Integer.parseInt(string)) {
		case AutomationItem.RINGER_NORMAL:
			am.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
			break;

		case AutomationItem.RINGER_SILENT:
			am.setRingerMode(AudioManager.RINGER_MODE_SILENT);
			break;

		case AutomationItem.RINGER_VIBRATE:
			am.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
			break;
		}
	}

	private static void doScreenBrightnessAction(Context context, String string) {
		// TODO
	}

	private static void doSMSAction(Context context, String string) {
		String[] str = string.split(",");
		Log.i("AutomationHandler->doSMSAction", str[0] + "body:" + str[1]);
		SmsMgr.SMSSend(context, str[0], str[1]);
	}
	
	public static void doAutoAfterUserPresent(Context context){
		ArrayList<AutomationItem> ailist = new ArrayList<AutomationItem>();
		ailist = AutoFileProcessor.getAutos(context);
		Iterator<AutomationItem> i = ailist.listIterator();
		AutomationItem ai;
		while(i.hasNext()){
			ai=i.next();
			if(ai.triggers.containsKey(AutomationItem.UNLOCK_TRIGGER)){
				Log.i("doAutoAfterUserPresent",""+ai.title);
				checkOtherTriggers(context, ai, -2);
			}
		}
	}
	
	public static void doAutoIncomingCallBR(Context context){
		ArrayList<AutomationItem> ailist = new ArrayList<AutomationItem>();
		ailist = AutoFileProcessor.getAutos(context);
		Iterator<AutomationItem> i = ailist.listIterator();
		AutomationItem ai;
		while(i.hasNext()){
			ai=i.next();
			if(ai.triggers.containsKey(AutomationItem.INCOMING_CALL_TRIGGER)){
				Log.i("doAutoIncomingCallBR",""+ai.title);
				checkOtherTriggers(context, ai, -2);
			}
		}
	}
}
