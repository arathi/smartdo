package com.smartdo;

import java.util.ArrayList;
import java.util.Collection;

import org.mapsforge.android.maps.MapView;
import org.mapsforge.android.maps.overlay.ItemizedOverlay;
import org.mapsforge.android.maps.overlay.OverlayItem;
import org.mapsforge.core.GeoPoint;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;

/*
 * This class extends the base ItemizedOverlay class
 * and overrides various methods like onTap etc.
 * @author Rashi, Prerna
 */
public class ArrayItemizedOverlay extends ItemizedOverlay<OverlayItem> {
	  private static final String THREAD_NAME = "ArrayItemizedOverlay";

	  private final Context context;
	  private AlertDialog.Builder dialog;
	  private OverlayItem item;
	  private final ArrayList<OverlayItem> overlayItems;
	  

	public ArrayItemizedOverlay(Drawable defaultMarker, Context context) {
	    super(defaultMarker == null ? null : boundCenterBottom(defaultMarker));
	    this.context = context;
	    this.overlayItems = new ArrayList<OverlayItem>();
	}
	public void addOverlay(OverlayItem overlayItem) {
	    synchronized (this.overlayItems) {
	      this.overlayItems.add(overlayItem);
	    }
	    populate();
	  }
	public void addOverlays(Collection<? extends OverlayItem> c) {
	    synchronized (this.overlayItems) {
	      this.overlayItems.addAll(c);
	    }
	    populate();
	  }

	public void clear() {
	    synchronized (this.overlayItems) {
	      this.overlayItems.clear();
	    }
	    populate();
	  }

	  @Override
	  public String getThreadName() {
	    return THREAD_NAME;
	  }
	public void removeOverlay(OverlayItem overlayItem) {
	    synchronized (this.overlayItems) {
	      this.overlayItems.remove(overlayItem);
	    }
	    populate();
	  }
	@Override
	public int size() {
	    synchronized (this.overlayItems) {
	      return this.overlayItems.size();
	    }
	  }
	@Override
	  protected OverlayItem createItem(int i) {
	    synchronized (this.overlayItems) {
	      return this.overlayItems.get(i);
	    }
	  }
	@Override
	  protected boolean onTap(int index) {
	    synchronized (this.overlayItems) {
	      this.item = this.overlayItems.get(index);
	      this.dialog = new AlertDialog.Builder(this.context);
	      this.dialog.setTitle(this.item.getTitle());
	      this.dialog.setMessage(this.item.getSnippet());
	      this.dialog.show();
	      return true;
	    }
	  }

	@Override
	public boolean onLongPress(GeoPoint p, MapView v){
		return false;
	}
}



