package com.smartdo;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

import org.mapsforge.android.maps.MapActivity;
import org.mapsforge.android.maps.MapView;

import android.os.Bundle;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.widget.Toast;

public class MapsProcessorTest extends MapActivity {
	ArrayItemizedOverlay arr=new ArrayItemizedOverlay(getResources().getDrawable(R.drawable.image), this);
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		MapView mapview = new MapView(this);
		mapview.setClickable(true);
		mapview.setBuiltInZoomControls(true);
		mapview.setMapFile(new File("/sdcard/Indore.map"));
		mapview.isFocusable();
		setContentView(mapview);
		mapview.getOverlays().add(arr);
		
		mapview.setLongClickable(true);
		mapview.setOnLongClickListener(new OnLongClickListener() {
			
			@Override
			public boolean onLongClick(View view) {
				Toast.makeText(getBaseContext(), "longClick!", Toast.LENGTH_SHORT).show();
				return false;
			}
		});
		mapview.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				if(arg1.getAction()==MotionEvent.ACTION_DOWN){
					Timer longpress=new Timer();
					longpress.schedule(new TimerTask(){
						@Override
						public void run(){
							Toast.makeText(getBaseContext(), "Click!", Toast.LENGTH_SHORT).show();
						}
					}, 5000);
				}
				
				return false;
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_maps_processor_test, menu);
		return true;
	}
}
