package com.smartdo;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
/**
 * Notification Manager - Pushing to the nofication bar.
 * 
 * @author aref
 *
 */
public class NotifiManager {
	public static void notify(Context context, String title, String content, int id){
		NotificationManager nm;
		nm=(NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification n = new Notification(R.drawable.ic_launcher, title, System.currentTimeMillis());
		n.defaults = Notification.DEFAULT_ALL;

//		Intent notificationIntent =intent;
//		notificationIntent.setClass(context, AlertAct.class);
//		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//		PendingIntent contentIntent = PendingIntent.getActivity(context, rcode/3, notificationIntent,  0);

		n.setLatestEventInfo(context, title, content, null);	
		nm.notify(id,n);
	}
}
