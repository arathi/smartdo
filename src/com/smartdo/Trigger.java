package com.smartdo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.SimpleTimeZone;
import java.util.concurrent.TimeUnit;

import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.smartdo.files.AutoFileProcessor;
/**
 *  @author Ashish
 *
 */
public class Trigger extends Activity {


	CheckBox ringer;
	CheckBox call;
	CheckBox battery;
	CheckBox unlock;
	CheckBox alarm;
	CheckBox location;
	CheckBox indoors;
	String ind="";

	RadioGroup ringergp;

	RadioButton ringer1;
	RadioButton ringer2;
	RadioButton ringer3;
	
	Spinner spinnerbattery;
	Spinner spinneralarm;

	int ringerflag1 = 0, ringerflag2 = 0, ringerflag3 = 0;
	int percent;
	int interval;
	int value;
	int flag=0;
	
	Button ok;

	int mYear, mMonth, mDay;
	int mHour, mMinute;
	Date date1;
	TextView timeView;
	Context context = this;
	AutomationItem ai;
	double lat=0;
	double longi=0;
	int indoorloc=0;
	String number;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_trigger);

		ai = (AutomationItem) getIntent().getSerializableExtra("trigger");
		/*
		 * LOOK AT THIS-
		 * 
		 **/
		if(!ai.triggers.isEmpty()){
			if(ai.triggers.get(AutomationItem.ALARM_TRIGGER)!=null){
				alarm = (CheckBox) findViewById(R.id.checkBoxAlarm);
				alarm.setChecked(true);
				timeView=(TextView) findViewById(R.id.TextViewtime);
				Long millis=Long.parseLong(ai.triggers.get(AutomationItem.ALARM_TRIGGER));			
//				//timeView.setText(date2.getHours()+date2.getMinutes());
//				String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
//			            TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
//			            TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
//				
				SimpleDateFormat df = new SimpleDateFormat("HH:mm");
				String formatted = df.format(millis);
				timeView.setText(formatted);
			}
			if(ai.triggers.get(AutomationItem.LOW_BATTERY_TRIGGER)!=null){
				battery=(CheckBox) findViewById(R.id.checkBoxBattery);
				battery.setChecked(true);
			}
			if(ai.triggers.get(AutomationItem.LOCATION_TRIGGER)!=null){
				location=(CheckBox) findViewById(R.id.checkBoxLocation);
				location.setChecked(true);
				TextView loc=(TextView) findViewById(R.id.triggerLocation);
				loc.setText(ai.triggers.get(AutomationItem.LOCATION_TRIGGER));
			}
			if(ai.triggers.get(AutomationItem.RINGER_MODE_TRIGGER)!=null){
				ringer=(CheckBox) findViewById(R.id.checkBoxRinger);
				ringer.setChecked(true);
				switch (Integer.parseInt(ai.triggers.get(AutomationItem.RINGER_MODE_TRIGGER))) {
				case AutomationItem.RINGER_NORMAL:
					ringer2=(RadioButton) findViewById(R.id.radioButtonRinger2);
					ringer2.setChecked(true);
					break;

				case AutomationItem.RINGER_SILENT:
					ringer1=(RadioButton) findViewById(R.id.radioButtonRinger1);
					ringer1.setChecked(true);
					break;

				case AutomationItem.RINGER_VIBRATE:
					ringer3=(RadioButton) findViewById(R.id.radioButtonRinger3);
					ringer3.setChecked(true);
					break;
				}

			}
		}

		ringergp = (RadioGroup) findViewById(R.id.radioGroupRingerT);
		ringer1 = (RadioButton) findViewById(R.id.radioButtonRinger1);
		ringer2 = (RadioButton) findViewById(R.id.radioButtonRinger2);
		ringer3 = (RadioButton) findViewById(R.id.radioButtonRinger3);
		timeView = (TextView) findViewById(R.id.autoTime);
		
		spinneralarm=(Spinner) findViewById(R.id.spinneralarm);
		spinnerbattery=(Spinner) findViewById(R.id.spinnerbattery);

		final Calendar c = Calendar.getInstance();
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);
		mHour = c.get(Calendar.HOUR_OF_DAY);
		mMinute = c.get(Calendar.MINUTE);

		date1 = new Date();

		ringer1.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (ringer1.isChecked()) {
					ringerflag1 = 1;

				}
			}
		});
		ringer2.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (ringer2.isChecked()) {
					ringerflag2 = 1;

				}
			}
		});
		ringer3.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (ringer3.isChecked()) {
					ringerflag3 = 1;

				}
			}
		});
		
		addItemOnBattery();
		addItemOnAlarm();

		ok = (Button) findViewById(R.id.buttonokt);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				doSave();
				
				finish();
			}});
		
	}

	public void addItemOnBattery(){
		List<Integer> listbattery=new ArrayList<Integer>();
		listbattery.add(40);
		listbattery.add(30);
		listbattery.add(25);
		listbattery.add(20);
		listbattery.add(15);
		listbattery.add(10);
		listbattery.add(5);
		
		ArrayAdapter<Integer> dataAdapter=new ArrayAdapter<Integer>(this,android.R.layout.simple_spinner_item,listbattery);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerbattery.setAdapter(dataAdapter);
	}
	
	public void addItemOnAlarm(){
		List<Integer> listalarm=new ArrayList<Integer>();
		for(int i=1;i<24;i++){
			listalarm.add(i);
		}
	
				ArrayAdapter<Integer> dataAdapter=new ArrayAdapter<Integer>(this,android.R.layout.simple_spinner_item,listalarm);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinneralarm.setAdapter(dataAdapter);
		
		
		
	}
	
	public void doSave() {

		ringer = (CheckBox) findViewById(R.id.checkBoxRinger);
		call = (CheckBox) findViewById(R.id.checkBoxCall);
		battery = (CheckBox) findViewById(R.id.checkBoxBattery);
		unlock = (CheckBox) findViewById(R.id.checkBoxUnlock);
		alarm = (CheckBox) findViewById(R.id.checkBoxAlarm);
		location = (CheckBox) findViewById(R.id.checkBoxLocation);
		indoors = (CheckBox) findViewById(R.id.checkBoxIndoors);


		if (ai == null) {
			Log.e("TOOOOASODOASD", "NULL!");
		}

		if (ringer.isChecked()) {
			if (ringerflag1 == 1) {
				ai.triggers.put(AutomationItem.RINGER_MODE_TRIGGER, ""
						+ AutomationItem.RINGER_SILENT);
			//	ai.triggertext="Ringer Mode:Silent"+"\n";
			}
			if (ringerflag2 == 1) {
				ai.triggers.put(AutomationItem.RINGER_MODE_TRIGGER, ""
						+ AutomationItem.RINGER_NORMAL);
			//	ai.triggertext="Ringer Mode:Normal"+"\n";
			}
			if (ringerflag3 == 1) {
				ai.triggers.put(AutomationItem.RINGER_MODE_TRIGGER, ""
						+ AutomationItem.RINGER_VIBRATE);
			//	ai.triggertext="Ringer Mode:Vibrate"+"\n";
			}
		}

		if (call.isChecked()) {
			number="8878874546";
			Log.i("Incomig call triigerr", "added");
			ai.triggers.put(AutomationItem.INCOMING_CALL_TRIGGER, number);
		//	ai.triggertext=ai.triggertext+" "+"Incoming Call:"+number+"\n";
		}

		if (battery.isChecked()) {
			percent=(Integer)spinnerbattery.getSelectedItem();
			ai.triggers.put(AutomationItem.LOW_BATTERY_TRIGGER, ""+percent);
		//	ai.triggertext=ai.triggertext+" "+"Low Battery:"+percent+"%"+"\n";
		}

		if (unlock.isChecked()) {
			ai.triggers.put(AutomationItem.UNLOCK_TRIGGER, "unlock");
		//	ai.triggertext=ai.triggertext+" "+"unlock phone"+"\n";
		}

		if (alarm.isChecked()) {
			interval= (Integer) spinneralarm.getSelectedItem();
			//value=Integer.parseInt(interval);//converted the interval in milliseconds
			interval=interval*3600000;
			ai.triggers.put(AutomationItem.ALARM_TRIGGER, ""+date1.getTime());
			ai.triggers.put(AutomationItem.ALARM_TRIGGER_MARGIN, ""+interval);	//one hour margin
			Log.i("Alarm", "" + ai.triggers.get(AutomationItem.ALARM_TRIGGER));
		//	ai.triggertext=ai.triggertext+" "+"Alarm:"+String.valueOf(interval)+"seconds"+"\n";
		}

		if (location.isChecked()) {
			
			flag=1;
			if(indoors.isChecked()){
				indoorloc=1;
				ind="indoors";
			}
			if(lat==0){
				Toast.makeText(context, "No location selected on map!", Toast.LENGTH_SHORT).show();
			}
				String location;
				location = lat + "," + longi +  "," + "500" + "," +indoorloc;	//TODO - Take this as input
				ai.triggers.put(AutomationItem.LOCATION_TRIGGER, location);
			//	ai.triggertext=ai.triggertext+" "+ind+" "+"Location:"+location+"\n";
			

		}
	}

	public void finish() {
		// doSave();
		Intent data=new Intent();
		data.putExtra("trigger", ai);
		setResult(1024, data);
		super.finish();
	}

	@SuppressWarnings("deprecation")
	public void showTimePickerTrigger(View view) {
		showDialog(999);
	}

	private TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {

		@SuppressWarnings("deprecation")
		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			mHour = hourOfDay;
			mMinute = minute;
			int h = mHour;
			date1.setHours(h);
			date1.setMinutes(minute);
			date1 = new Date(mYear - 1900, mMonth, mDay, mHour, mMinute);
			timeView.setText(" " + mHour + ":" + mMinute);
			Log.e("dt: ", mHour +" "+mMinute);
			Log.d("DT: ", date1.toString());
		}
	};

	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case 999:
			return new TimePickerDialog(this, mTimeSetListener, mHour, mMinute,
					false);
		}
		return null;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_trigger, menu);
		return true;
	}

	public void viewMap(View view) {
		Intent intent = new Intent(this, MapProcessor.class);
		intent.putExtra("action", "automation");
		startActivityForResult(intent, 6000);
	} 

	public void viewMap() {
		Intent intent = new Intent(this, MapProcessor.class);
		intent.putExtra("action", "automation");
		startActivityForResult(intent, 6000);
	} 
	
	@Override
	public void onActivityResult(int reqcode, int rescode, Intent data) {
		if(rescode==5001 && reqcode==6000){
			onResume();
			lat = data.getDoubleExtra("lat", 0);
			longi = data.getDoubleExtra("longi", 0);
			TextView loc=(TextView)findViewById(R.id.triggerLocation);
			loc.setText(lat+" "+longi);

		}
	}
	

}
